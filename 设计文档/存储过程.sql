create PROCEDURE [dbo].[proc_addUserRole]
	 @uid int,  -- 用户id
	 @rids nvarchar(500) -- 权限id，字符串
AS
BEGIN
	DECLARE @rid nvarchar(200)
	DECLARE @splitchar nvarchar(1)
	SET @splitchar=','
	SET @rids=@rids+@splitchar

	-- 先删除
	begin tran
	BEGIN TRY
	    DELETE FROM UserRoles WHERE UserId=@uid

		while CHARINDEX(@splitchar,@rids)>0 -- 循环条件
		begin
			set @rid = SUBSTRING(@rids,1,CHARINDEX(@splitchar,@rids)-1)
			if(RTRIM(LTRIM(@rid)) <> '')
			begin
				insert into UserRoles values(@uid,@rid)
			end
			set @rids = RIGHT(@rids,LEN(@rids)-CHARINDEX(@splitchar,@rids))
		end

		COMMIT tran
	END TRY
	BEGIN CATCH
		ROLLBACK tran
	END CATCH
END
GO
Create proc [dbo].[proc_LoadUserMenu] 
( 
	@userid varchar(50) 
)
as
begin
	select * from Permissions where Pid in -- 获取具体的菜单数据 
	( 
		select PermissionID from RolePermissions where RoleID in -- 获取角色拥有的权限菜单 
		( 
			select RoleID from UserRoles where UserId= @userid -- 获取用户的 角色 
		) 
	) 
end