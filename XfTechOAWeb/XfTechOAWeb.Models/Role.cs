﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 角色
    /// </summary>
    public class Role
    {
        [Key]
        public int RId { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [StringLength(30)]
        [Required]
        public string RName { get; set; }

        /// <summary>
        /// 角色描述
        /// </summary>
        [StringLength(100)]
        [Required]
        public string RText { get; set; }

        /// <summary>
        /// 状态 0：禁用、 1： 启用
        /// </summary>
        public int Status { get; set; } = 1;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建人
        /// </summary>
        public int CreatedUserID { get; set; }
    }
}
