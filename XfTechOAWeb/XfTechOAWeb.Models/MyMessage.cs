﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 我的消息(用户通知消息表)
    /// </summary>
    public class MyMessage
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public int MId { get; set; }

        /// <summary>
        /// 公告Id
        /// </summary>
        public int NoticeId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 状态,已读或者未读(0未读1已读)
        /// </summary>
        public int MessageState { get; set; }

        /// <summary>
        /// 消息创建时间
        /// </summary>
        public DateTime MessageDate { get; set; }

        /// <summary>
        /// 消息阅读时间
        /// </summary>
        public DateTime? MessageReadDate { get; set; }

    }
}
