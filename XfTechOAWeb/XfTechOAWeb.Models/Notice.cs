﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;

namespace XfTechOAWeb.Models
{
    public enum NoticeUserType
    {
        发送给所有人=1,
        发送给指定角色=2,
        发送给指定部门=3,
        发送给个人=4
    }

    public enum NoticeStatus
    {
        正常 = 1,
        撤销 = 2,
        删除 = 3
    }

    /// <summary>
    /// 通知公告模型
    /// </summary>
    public class Notice
    {
        /// <summary>
        /// 主键id
        /// </summary>
        [Key]
        public int NoticeId { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [StringLength(50)]
        [Required]
        public string NoticeTitle { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public NoticeUserType UserType { get; set; } = NoticeUserType.发送给所有人;

        /// <summary>
        /// 发送给谁（如果是发送给所有人，则留空）
        /// </summary>
        [StringLength(20)]
        public string Receiver { get; set; }

        /// <summary>
        /// 公告内容
        /// </summary>
        [Required]
        public string NoticeContent { get; set; }

        /// <summary>
        /// 状态（这条消息是否被撤回，会被删除）
        /// </summary>
        public NoticeStatus Status { get; set; } = NoticeStatus.正常;

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime CreatedTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 发布人Id
        /// </summary>
        public int CreatedUserID { get; set; }

        /// <summary>
        /// 发布人名称
        /// </summary>
        public string CreatedUser { get; set; }

        /// <summary>
        /// 最后修改时间
        /// </summary>
        public DateTime? LastModifiedTime { get; set; }

        /// <summary>
        /// 最后修改人
        /// </summary>
        public int? LastModifiedUserID { get; set; }
    }
}
