﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Models
{
    [Table("OperationLog")]
    public class OperationLog//操作日志表
    {
        [Key]
        public int OLid { get; set; }//操作日志ID
        public string Content { get; set; }//内容
        public string Classify { get; set; }//分类
        public string Module { get; set; }//模块
        public bool Whether { get; set; }//是否异常
        public string Use { get; set; }//应用名称
        public string OperatorName { get; set; }//操作人
        public DateTime CreateTimes { get; set; } = DateTime.Now;//创建时间
    }
}
