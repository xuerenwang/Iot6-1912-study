﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 表单设计表
    /// </summary>
    [Table("Forms")]
    public class Form
    {
        /// <summary>
        /// 主键id
        /// </summary>
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 表单名称
        /// </summary>
        [Required]
        [StringLength(50)]
        public string FormName { get; set; }
        /// <summary>
        /// 表单数据
        /// </summary>
        [Required]
        [MaxLength]
        public string FormJson { get; set; }

        /// <summary>
        /// 表单描述
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        /// <summary>
        /// 表单创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }= DateTime.Now;
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateName { get; set; }
    }
}
