﻿/*
 * 创建人：金鑫
 * 创建时间：2022/8/26
 * 
 * *******************************************************************
 * 
 * 功能描述：定时器Job实体
 * 
 * *******************************************************************
 * 修改履历：
 * 
 * *******************************************************************
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 定时任务管理实体类
    /// </summary>
    public class Timing
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        [Required]
        [StringLength(30)]
        public string JobName { get; set; }
        /// <summary>
        /// 任务分组
        /// </summary>
        [Required]
        [StringLength(30)]
        public string JobGroup { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTimeOffset BeginTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTimeOffset? EndTime { get; set; }
        /// <summary>
        /// Cron字符串
        /// </summary>
        [StringLength(30)]
        public string Cron { get; set; }
        /// <summary>
        /// 执行次数
        /// </summary>
        public int? RumTimes { get; set; }

        /// <summary>
        /// 执行间隔时间，单位秒(如果使用的是Cron,则失效)
        /// </summary>
        public int? IntervalSecond { get; set; }

        /// <summary>
        /// 触发器类型
        /// </summary>
        [StringLength(20)]
        public string TriggerType { get; set; }
        /// <summary>
        /// 请求Url
        /// </summary>
        [StringLength(200)]
        public string RequestUrl { get; set; }
        /// <summary>
        /// 命名名称
        /// </summary>
        [StringLength(200)]
        public string Namespace { get; set; }
        /// <summary>
        /// 是否持久化
        /// </summary>
        public bool Yun { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [StringLength(100)]
        public string Description { get; set; }
        /// <summary>
        /// Job任务的状态
        /// </summary>
        public int Zt { get; set; } = 0;//o禁用,1启用中

    }
}
