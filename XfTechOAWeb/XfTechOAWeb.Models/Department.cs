﻿/*
 * 创建人：申永康
 * 创建时间：2022/8/16
 * 
 * *******************************************************************
 * 
 * 功能描述：部门表
 * 
 * *******************************************************************
 * 修改履历：
 * 申永康 20220816 创建文件
 * 申永康 20220816 添加注释
 * 
 * *******************************************************************
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 部门实体类
    /// </summary>
    public class Department
    {
        [Key]
        public int DepId { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        [StringLength(20)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 层级Id
        /// </summary>
        [Required]
        [StringLength(20)]
        public string CascadeId { get; set; } = "0";

        /// <summary>
        /// 上级部门Id
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 状态 0：禁用、 1： 启用
        /// </summary>
        public int Status { get; set; } = 1;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建人
        /// </summary>
        public int CreatedUserID { get; set; } = 1;
        public List<Department> Dlist { get; set; }
        /// <summary>
        /// 逻辑删除
        /// </summary>

        public int IsChecks { get; set; } = 0;

    }
}
