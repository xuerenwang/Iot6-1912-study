﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Models.Base
{
    /// <summary>
    /// 带审计字典的实体
    /// </summary>
    public abstract class AuditedEntity
    {
        /// <summary>
        /// 创建人
        /// </summary>
        [StringLength(50)]
        [Required]
        public string? CreateUser { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        /// 
        [StringLength(50)]
        [Required]
        public string? UpdateUser { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
}
