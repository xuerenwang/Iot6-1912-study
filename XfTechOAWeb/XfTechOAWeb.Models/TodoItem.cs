﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 待办事项实体类
    /// </summary>
    public class TodoItem
    {
        [Key]
        public int ItemId { get; set; }

        /// <summary>
        /// 待办事项
        /// </summary>
        [StringLength(100)]
        [Required]
        public string Text { get; set; }

        /// <summary>
        /// 状态 0：待办、 1： 已完成
        /// </summary>
        public bool Status { get; set; } = true;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建人
        /// </summary>
        public int CreatedUserID { get; set; }
    }
}
