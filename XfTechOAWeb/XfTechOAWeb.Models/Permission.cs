﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 权限实体类
    /// </summary>
    public class Permission
    {
        [Key]
        public int PId { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [StringLength(50)]
        [Required]
        public string PTitle { get; set; }

        /// <summary>
        /// 权限标识符
        /// </summary>
        [StringLength(255)]
        public string PAction { get; set; }

        /// <summary>
        /// 父级Id
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// URL路径
        /// </summary>
        [StringLength(255)]
        public string PApiUrl { get; set; }

        /// <summary>
        /// 它是不是前台菜单  true：表示是前端显示菜单， false:表示前端不显示菜单
        /// </summary>
        public bool IsMenu { get; set; }

        /// <summary>
        /// 菜单图标名称(可空）
        /// </summary>
        [StringLength(50)]
        public string IconName { get; set; }

        /// <summary>
        /// 排序号(越大优先级越高）
        /// </summary>
        public int SortNo { get; set; }
    }
}
