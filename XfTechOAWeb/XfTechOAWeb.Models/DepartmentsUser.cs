﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 部门角色表
    /// </summary>
    [Table("DepartmentsUser")]
    public class DepartmentsUser
    {
        [Key]
        //部门用户ID
        public int DUId { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        public int DepId { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UId { get; set; }
    }
}
