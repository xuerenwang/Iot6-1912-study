﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models.Base;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 字典值
    /// </summary>
    [Table("DictValues")]
    public class DictValue : AuditedEntity
    {
        /// <summary>
        /// 数据字典从表主键
        /// </summary>
        [Key]
        public int DictValueId { get; set; }
        /// <summary>
        /// 数据字典从表
        /// </summary>
        /// 
        [StringLength(50)]
        [Required]
        public string DictValueName { get; set; }

        /// <summary>
        /// 字典表ID
        /// </summary>
        public int DictKeyId { get; set; }
       
        /// <summary>
        /// 数据字典从表状态
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 数据字典从表代码
        /// </summary>
        [StringLength(50)]
        [Required]
        public string DictValueCode { get; set; }
    }
}
