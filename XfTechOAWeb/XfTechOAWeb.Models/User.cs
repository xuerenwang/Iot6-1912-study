﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 用户实体类
    /// </summary>
    public class User
    {
        [Key]
        public int UId { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        [StringLength(50)]
        [Required]
        public string Account { get; set; }

        /// <summary>
        /// 密码（MD5哈希字符串）
        /// </summary>
        [StringLength(50)]
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        [StringLength(50)]
        [Required]
        public string UName { get; set; }

        /// <summary>
        /// 状态 0：禁用、 1： 启用
        /// </summary>
        public bool Status { get; set; } = true;

        /// <summary>
        /// 部门Id （此项目中设计是：一个部门对多个用户，即一对多关系）
        /// </summary>
        public string DepId { get; set; }

        /// <summary>
        /// 逻辑删除（默认为false）
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建人
        /// </summary>
        public int CreatedUserID { get; set; }
    }
}
