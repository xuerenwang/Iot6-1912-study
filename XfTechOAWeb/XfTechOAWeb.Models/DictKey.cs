﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models.Base;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 字典主表
    /// </summary>
    [Table("DictKeys")]
    public class DictKey : AuditedEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public int DictKeyId { get; set; }

        /// <summary>
        /// 主表名称
        /// </summary>
        [StringLength(50)]
        [Required]
        public string DictKeyName { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 代码
        /// </summary>
        [StringLength(30)]
        [Required]
        public string DictKeyCode { get; set; }

    }
}
