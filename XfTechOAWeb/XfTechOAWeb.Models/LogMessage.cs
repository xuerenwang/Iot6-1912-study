﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Models
{
   /// <summary>
   /// 日志消息模型
   /// </summary>
   public class LogMessage
    {
        [Key]
        //主键
        public int NId { get; set; }
        /// <summary>
        /// 请求用户
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 打印耗时
        /// </summary>
        public string OperationConsume { get; set; }
        /// <summary>
        /// 生成时间
        /// </summary>
        public DateTime GeneratedTime { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public string Site { get; set; }
        /// <summary>
        /// 控制器名
        /// </summary>
        public string OperationClass { get; set; }
        /// <summary>
        /// 执行的方法
        /// </summary>
        public string MethodName { get; set; }
        /// <summary>
        /// 请求方式
        /// </summary>
        public string WayToRequest { get; set; }
        /// <summary>
        /// 参数类
        /// </summary>
        public string Parameter { get; set; }
        /// <summary>
        /// 打印类容
        /// </summary>
        public string LogMasg { get; set; }

    }
}
