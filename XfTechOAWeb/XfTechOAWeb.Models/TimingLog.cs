﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Models
{
    /// <summary>
    /// 定时器日志
    /// </summary>
  public  class TimingLog
    {
        /// <summary>
        /// Key
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 当前时间
        /// </summary>
        public DateTime StartTime{ get; set; }
        /// <summary>
        ///所属Job分区信息
        /// </summary>
        public string PGroupName { get; set; }
        /// <summary>
        /// 显示状态
        /// </summary>
        public bool Zt { get; set; } = true;

    }
}
