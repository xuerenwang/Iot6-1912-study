﻿using XfTechOAWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Data.Base;

namespace XfTechOAWeb.Data
{
    public interface IRoleRepository : IRepository<Role>
    {

        int AddRolePermission(int rid, string pid);  //给角色添加权限

        List<RolePermission> GetRolePermissions(int rid);

        List<UserRole> GetRoleUsers(int rid);

        List<Role> GetRolesFromSql(); //子类的方法
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        int Add(Role role);
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        int Upt(Role role);
        /// <summary>
        /// 批删
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        int Del(string ids);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        PageResponseModel<Role> Show(int pageIndex, int pageSize, string name);
        /// <summary>
        /// 获取所有的权限
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        List<PermissionTreeDto> PTree(int pid);
        //(int count, List<Role> roles) GetRoleList(int pindex = 1, int psize = 2);

    }
}
