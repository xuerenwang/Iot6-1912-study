﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models;
using XfTechOAWeb.Dtos.Timing;
using XfTechOAWeb.Data.Base;

namespace XfTechOAWeb.Data
{
    public interface ITimingRepository : IRepository<Timing>
    {
        bool UptZt(int id, int status);
        bool Remove(int id);
        bool Insert(TimingLog  Logs);
        bool Finish(string GroupName);
    }
}
