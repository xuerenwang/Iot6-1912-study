﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Data.Base
{
    /// <summary>
    /// 工作单元接口
    /// </summary>
    public interface IUnitOfWork
    {
        int SaveChanges();             //保存提交

        Task<int> SaveChangesAsync();  // 异步的保存提交
    }
}
