﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Data.Base
{
    public interface IUnitOfWorkManage
    {
        int TranCount { get; }

        void BeginTran();
        
        void CommitTran();

        void RollbackTran();

        //void CommitTran(MethodInfo method);
        //void BeginTran(MethodInfo method);
        //void RollbackTran(MethodInfo method);
    }
}
