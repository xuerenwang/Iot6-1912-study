﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos.Departments;

namespace XfTechOAWeb.Data.Base
{
    /// <summary>
    /// 泛型仓储接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class
    {
        void Insert(T model);  //增

        void Update(T model);  //改

        void Remove(object id); //删
        int Del(string id);
        int Upt(int id);
        int Upt(UptDepartmentsDto id);
        IQueryable<T> Table { get; } //查

        T GetById(object id); //通过主键查找

        int SaveChanges();  //保存修改

        Task<int> SaveChangesAsync();  //保存修改

        IEnumerable<dynamic> QueryFromSql(string sql, params DbParameter[] parameters); //执行sql语句 获取多表联查字段
        IEnumerable<dynamic> QueryFromProc(string procName, params DbParameter[] parameters); //执行存储过程 获取多表联查字段

        int ExecuteFromSql(string sql, params DbParameter[] parameters);       //执行sql语句 （增、删、改）
        int ExecuteFromProc(string procName, params DbParameter[] parameters); //执行存储过程 （增、删、改）
    }
}
