﻿using XfTechOAWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Data.Base;

namespace XfTechOAWeb.Data
{
    public interface IUserRepository : IRepository<User>
    {
        int AddUserRole(int uid, string rid); //给用户添加角色


        //C#里面的dynamic是动态类型
        IEnumerable<dynamic> LoadUserMenu(int userid);
    }
}
