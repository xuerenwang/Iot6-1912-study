﻿/*
 * 创建人：申永康
 * 创建时间：2022/8/16
 * 
 * *******************************************************************
 * 
 * 功能描述：部门仓储
 * 
 * *******************************************************************
 * 修改履历：
 * 申永康 20220816 创建文件
 * 申永康 20220816 添加注释
 * 
 * *******************************************************************
 */
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Data.Base;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Departments;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Data
{
    public interface IDepartmentRepository : IRepository<Department>
    {
        /// <summary>
        /// 部门添加
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        ResponseModel<AddDepartmentsDto> Add(AddDepartmentsDto department);
        /// <summary>
        /// 部门树形显示
        /// </summary>
        /// <returns></returns>
        List<Department> Show();
        /// <summary>
        /// 部门分页
        /// </summary>
        /// <param name="Pindex"></param>
        /// <param name="Psize"></param>
        /// <returns></returns>
       
        PageResponseModel<DepartmentDto> DShow(int Pindex = 1, int Psize = 2);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Dels(string id);
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Upts(int id);

        /// <summary>
        /// 为部门添加用户
        /// </summary>
        /// <param name="depId"></param>
        /// <param name="uId"></param>
        /// <returns></returns>
        int AddDepartmentsUser(int depId,string uId);
    }
}
