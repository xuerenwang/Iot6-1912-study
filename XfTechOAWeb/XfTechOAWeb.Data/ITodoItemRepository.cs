﻿using XfTechOAWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace XfTechOAWeb.Data
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITodoItemRepository
    {
        Task<IEnumerable<TodoItem>> GetListAsync(int userId);
        Task<int> InsertAsync(TodoItem todoItem);
    }
}
