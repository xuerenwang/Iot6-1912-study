﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Data.Base;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Data
{
    /// <summary>
    /// 表单设计接口仓储
    /// </summary>
    public interface IFormDesignerRepository: IRepository<Form>
    {
       /// <summary>
       /// 显示所有
       /// </summary>
       /// <returns></returns>
        List<Form> GetAllFormDesiSgner();
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="formDesigner"></param>
        /// <returns></returns>
        int AddFormDesigner(Form formDesigner);
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="formDesigner"></param>
        /// <returns></returns>
        int UpdateFormDesigner(Form formDesigner);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteFormDesigner(string ids);
        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        Form GetFormDesigner(int id);


    }
}
