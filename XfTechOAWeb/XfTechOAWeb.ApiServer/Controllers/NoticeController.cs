﻿using Autofac.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System;
using XfTechOAWeb.Dtos.Notices;
using XfTechOAWeb.IService;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Infrastructure.Enums;

namespace XfTechOAWeb.Controllers
{
    /// <summary>
    /// 公告通知管理模块
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class NoticeController : ControllerBase
    {
        private readonly INoticeService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public NoticeController(INoticeService service)
        {
            _service = service;
        }

        /// <summary>
        /// 发送消息通知
        /// </summary>
        /// <returns></returns>
        [HttpPost("SendNotice")]
        public IActionResult SendNotice([FromBody] NoticeAddDto dto)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new { Code = 400, Msg = "数据错误，请重试" });
            }

            //添加到数据库中
            var result = _service.AddNotice(dto);
            
            return Ok(new ResponseModel<int> { 
                Code = 200, 
                Msg = result>0?"添加成功":"添加失败", 
                Data = result });
        }

        /// <summary>
        /// 添加通知
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddNotice(NoticeAddDto dto)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new { Code = 400, Msg = "参数格式有误", Data = dto });
            }

            //添加到数据库中
            var result = _service.AddNotice(dto);

            return Ok(new ResponseModel<int>
            {
                Code = 200,
                Msg = result > 0 ? "添加成功" : "添加失败",
                Data = result
            });
        }

        /// <summary>
        /// 分页显示
        /// </summary>
        /// <param name="pageDto"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetNoticeByPage([FromQuery] NoticePageDto pageDto)
        {
            var result = _service.GetNoticeByPage(pageDto);
            if(result == null)
            {
                return Ok(new ResponseModel<object> { Code = 204, Msg = "没有查询到数据", Data = null });
            }

            return Ok(new ResponseModel<object> { Code = 200, Msg = "查询成功", Data = result });
        }
    }
}
