﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using XfTechOAWeb.IService;
using XfTechOAWeb.Infrastructure.JWT;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Infrastructure.RedisCache;
using XfTechOAWeb.Infrastructure.Enums;
using XfTechOAWeb.Infrastructure.Extensions;
using System.Security.Claims;
using XfTechOAWeb.Models;
using XfTechOAWeb.Dtos.TodoItems;


namespace XfTechOAWeb.Controllers
{
    /// <summary>
    /// 待办事项管理
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemController : ControllerBase
    {
        private readonly ITodoItemService _todoItemService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="todoItemService"></param>
        public TodoItemController(ITodoItemService todoItemService)
        {
            _todoItemService = todoItemService;
        }
        
        /// <summary>
        /// 获取用户的待办事项
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            //获取用户主键
            var userIdClaim = HttpContext.User.FindFirst(s => s.Type == ClaimTypes.NameIdentifier);
            int userId;
            if (!int.TryParse(userIdClaim.Value, out userId))
            {
                return BadRequest();
            }
            //获取用户的待办事项
            var result = await _todoItemService.GetListAsync(userId);

            return Ok(new ResponseModel<object>
            {
                Code = (int)ResponseCode.Success,
                Msg = ResponseCode.Success.GetDescription(),
                Data = result
            });
        }

        /// <summary>
        /// 添加待办事项
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(CreateUpdateTodoItemDto input)
        {
            //获取用户的待办事项
            var result = await _todoItemService.InsertAsync(input);

            return Ok(new ResponseModel<object>
            {
                Code = result> 0 ? (int)ResponseCode.Success: (int)ResponseCode.Fail,
                Msg = ResponseCode.Success.GetDescription(),
                Data = input
            });
        }

    }
}
