﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Collections.Generic;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Forms;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.ApiServer.Controllers
{
    /// <summary>
    /// 表单管理模块
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FormController : ControllerBase
    {
        private readonly IFormDesignerService _formDesignerService;

        /// <summary>
        /// 构造函数注入
        /// </summary>
        /// <param name="formDesignerService"></param>
        public FormController(IFormDesignerService formDesignerService)
        {
            _formDesignerService = formDesignerService;
        }

        /// <summary>
        /// 创建表单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create(FormDto input)
        {
            int result = _formDesignerService.AddFormDesigner(input);

            return Ok(result);
        }

        //[HttpPut]
        //public int Edit([FromBody] FormDto input)
        //{
        //    var forms = _formDesignerService.GetFormDesigner(input.Id);

        //    forms.FormName = input.FormName;
        //    forms.CreateName = createName;
        //    forms.FormJson = input.FormJson1.ToString();
        //    forms.UpdateTime = System.DateTime.Now;
        //    return Update(forms);
        //}

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public ResponseModel<int> Delete(string ids)
        {
            return _formDesignerService.DeleteFormDesigner(ids);
        }

        /// <summary>
        /// 显示
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ResponseModel<PageResponseModel<FormShowDto>> Get(string tableName, int pageIndex = 1, int pageSize = 10)
        {
            var list = _formDesignerService.GetAllFormDesiSgner(tableName, pageIndex, pageSize);
            return list;
        }

        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("FindSingle")]
        public IActionResult Get(int id)
        {
            var form = _formDesignerService.GetFormDesigner(id);
            return Ok(form);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Update(FormDto input)
        {
            ResponseModel<int> res = _formDesignerService.UpdateFormDesigner(input);
            return Ok(res);

        }
    }
}
