﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using XfTechOAWeb.IService;
using XfTechOAWeb.Service;
using XfTechOAWeb.Dtos.Departments;
using Microsoft.AspNetCore.Authorization;
using XfTechOAWeb.Dtos;

namespace XfTechOAWeb.ApiServer.Controllers
{
    /// <summary>
    /// 部门管理
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentService _departmentService;

        /// <summary>
        /// 依赖注入
        /// </summary>
        /// <param name="departmentService"></param>
        public DepartmentController(IDepartmentService departmentService)
        {
            this._departmentService = departmentService;
        }
        /// <summary>
        /// 部门添加
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Add(AddDepartmentsDto addDto)
        {
            //模型验证
            if (!ModelState.IsValid)
            {
                return Ok("数据未传入");
            }
            else
            {
                ResponseModel<AddDepartmentsDto> responseModel = _departmentService.Add(addDto);
                return Ok(responseModel);
            }
        }
        /// <summary>
        /// 部门修改
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Update(UptDepartmentsDto uptDepartmentsDto)
        {
            //模型验证
            if (!ModelState.IsValid)
            {
                return Ok("数据未传入");
            }
            else
            {
                ResponseModel<UptDepartmentsDto> responseModel = _departmentService.Update(uptDepartmentsDto);
                return Ok(responseModel);
            }
        }
        /// <summary>
        /// 部门分页显示
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult DShow([FromQuery]DepartmentPageDto departmentPageDto)
        {
            var ls= _departmentService.DShow(departmentPageDto);
            return Ok(new {data=ls,msg="显示成功!" });
        }
        /// <summary>
        /// 部门树形显示
        /// </summary>
        /// <returns></returns>
        [HttpGet("getTree")]
        public IActionResult Show()
        {
            return Ok(new { data = _departmentService.GetDepartTree().ToList(), msg = "显示成功!" });
        }
        /// <summary>
        /// 部门的删除
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Del(string id)
        {
            _departmentService.Del(id);
            return Ok(new { data = 1, msg = "删除成功!" });
        }
        /// <summary>
        /// 部门的停用
        /// </summary>
        /// <returns></returns>
        [HttpPut("disable/{id}")]
        public IActionResult ChangeStatus(int id)
        {
            _departmentService.Upt(id);
            return Ok(new { data = 1, msg = "停用成功!" });
        }
        /// <summary>
        /// 给部门添加用户
        /// </summary>
        /// <param name="addDepartmentsUsersDto"></param>
        /// <returns></returns>
        [HttpPost("addDepartmentUsers")]
        public IActionResult AddDepartmentsUser(AddDepartmentsUsersDto addDepartmentsUsersDto)
        {
            return Ok(new { data = _departmentService.AddDepartmentsUser(addDepartmentsUsersDto) });
        }
        /// <summary>
        /// 返回以分配到该部门的用户
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}/getUsers")]
        public IActionResult BackDepartmen(int id)
        {
            return Ok(new { data = _departmentService.GetDepartment(id) });
        }

        /// <summary>
        /// 部门修改返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult BackUpt(int id)
        {
            return Ok(new {data=_departmentService.GIt(id) });
        }
    }
}
