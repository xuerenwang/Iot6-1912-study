﻿/*
 * 创建人：金鑫
 * 创建时间：2022/8/26
 * 
 * *******************************************************************
 * 
 * 功能描述：定时任务控制器
 * 
 * *******************************************************************
 * 修改履历：
 * 
 * *******************************************************************
 */
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos.Timing;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.ApiServer.Controllers
{
    /// <summary>
    /// 定时任务控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TimingController : ControllerBase
    {
        private readonly ITimingService _timingService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="timingService"></param>
        public TimingController(ITimingService timingService)
        {
            _timingService = timingService;
        }

        #region  数据库操作

        [HttpGet]
        public BaseResult CorrectJob()
        {
            return _timingService.CorrectJob();
        }

        /// <summary>
        /// 获取数据库全部Job任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpGet]
        public PageDto Timing([FromQuery]TimingPage data)
        {
            return _timingService.GetTimingList(data);
        }

        /// <summary>
        /// 删除数据库Job任务
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public BaseResult Timing(int id)
        {
            return _timingService.DeleteTiming(id);
        }

        /// <summary>
        /// 对数据库添加Job任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost("/Api/AddTiming")]
        public BaseResult Timing(Timing data)
        {
            return _timingService.AddTiming(data);
        }
        #endregion

        #region 调度器部分

        /// <summary>
        /// 启动Job任务
        /// </summary>
        /// <param name="data">Job信息</param>
        /// <returns></returns>
        [HttpPost]
        public Task<BaseResult> ScheduleJob(TimingDto data)
        {
            return _timingService.StartScheduleJob(data);
        }

        /// <summary>
        /// 暂停Job任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpDelete]
        public Task<BaseResult> ScheduleJob(DeleteJobDto data)
        {
            return _timingService.PauseScheduleJob(data);
        }

        /// <summary>
        /// 恢复被暂停的Job任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPut]
        public Task<BaseResult> ScheduleJob(StartJobDto data)
        {
            return _timingService.RecoverScheduleJob(data);
        }
        #endregion


        #region   反射
        /// <summary>
        /// 反射获取全部继承了IJOb接口的类的命名路由
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<dynamic> AfterRouting()
        {
            return _timingService.After();
        }
        #endregion

    }
}
