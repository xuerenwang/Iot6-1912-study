﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.MyMessages;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.ApiServer.Controllers
{
    /// <summary>
    /// 我的系统消息
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]   
    public class SysMessageController : ControllerBase
    {
        private readonly IMyMessageService _service;
        /// <summary>
        /// 依赖注入
        /// </summary>
        /// <param name="service"></param>
        public SysMessageController(IMyMessageService service)
        {
            _service = service;
        }

        /// <summary>
        /// 分页查询（只获取当前登录用户的消息）
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetList([FromQuery] MyMessagePageDto input)
        {
            var result = _service.GetMessageByPage(input);

            return Ok(new ResponseModel<object> { Code=200, Msg = "查询成功", Data=result });
            
        }

        /// <summary>
        /// 修改成已读
        /// </summary>
        /// <param name="id"></param>
        /// <param name="opt"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Upt(int id, int status=1)
        {
            int result = _service.UpdateStatus(id, status);
            if (result == 0)
            {
                return Ok(new ResponseModel<object> { Code = 302, Msg = "状态无法修改为已读", Data = result });
            }

            return Ok(new ResponseModel<object>() { Code = 200, Msg = "状态修改为已读", Data = result });
        }

        /// <summary>
        /// 获取公告内容
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            ResponseModel<object> obj = new ResponseModel<object>();
            var result = _service.GetMessageId(id);
            if (result == null)
            {
                return Ok(new { Code = 302, Msg = "获取id失败", Data = result });
            }

            return Ok(new { Code = 300, Msg = "获取id成功", Data = result });
        }

        /// <summary>
        /// 获取未读消息数量
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetUnReadCount")]
        public IActionResult GetUnReadCount()
        {
            int result = _service.GetUnReadCount();
            return Ok(new ResponseModel<int> { Code=200, Msg="获取成功", Data=result });
        }
    }
}
