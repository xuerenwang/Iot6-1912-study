﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Dicts;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;
using XfTechOAWeb.Service;

namespace XfTechOAWeb.ApiServer.Controllers
{
    /// <summary>
    /// 字典值管理模块
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DictValueController : ControllerBase
    {
        public readonly IDictValueService _dictService;

        /// <summary>
        /// 依赖注入
        /// </summary>
        /// <param name="dictService"></param>
        public DictValueController(IDictValueService dictService)
        {
            _dictService = dictService;
        }

        /// <summary>
        /// 数据字典显示
        /// </summary>
        /// <returns></returns>
        [HttpGet("getList")]
        public IActionResult GetList([FromQuery] DictValuePageRequestDto input)
        {
            //获取列表（不分页，无条件）
            var list = _dictService.Query(input);

            return Ok(new ResponseModel<object>
            {
                Code = StatusCodes.Status200OK,
                Data = list,
                Msg = "操作成功"
            });
        }

        /// <summary>
        /// 数据字典添加
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Add(DictValueAddUpdateDto input)
        {
            //模型验证
            if (!ModelState.IsValid)
            {
                return BadRequest(input); //返回400
            }

            int flag = _dictService.Insert(input);

            return Ok(new ResponseModel<int>
            {
                Code = StatusCodes.Status200OK,
                Data = flag,
                Msg = flag > 0 ? "添加成功" : "添加失败"
            });
        }

        /// <summary>
        /// 数据字典删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            int flag = _dictService.Delete(id);
            return Ok(new ResponseModel<int>
            {
                Code = StatusCodes.Status200OK,
                Data = flag,
                Msg = flag > 0 ? "删除成功" : "删除失败"
            });
        }

        /// <summary>
        /// 数据字典修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateDict(DictValueAddUpdateDto input)
        {
            //模型验证
            if (!ModelState.IsValid)
            {
                return BadRequest(input); //返回400
            }

            int flag = _dictService.Update(input);
            return Ok(new ResponseModel<int>
            {
                Code = StatusCodes.Status200OK,
                Data = flag,
                Msg = flag > 0 ? "修改成功" : "修改失败"
            });

        }
        /// <summary>
        /// 数据字典反填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getById/{id}")]
        ///反填
        public IActionResult Get(int id)
        {
            if (id == 0)
            {
                throw new ArgumentOutOfRangeException("无效的Id");
            }

            return Ok(new ResponseModel<object>
            {
                Code = StatusCodes.Status200OK,
                Data = _dictService.GetById(id),
                Msg = "操作成功"
            });
        }
    }
}
