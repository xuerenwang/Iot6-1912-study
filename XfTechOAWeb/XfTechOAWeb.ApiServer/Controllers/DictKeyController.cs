﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Dicts;
using XfTechOAWeb.EFCore;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;
using static ICSharpCode.SharpZipLib.Zip.ExtendedUnixData;

namespace XfTechOAWeb.ApiServer.Controllers
{
    /// <summary>
    /// 数据字典模块
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DictKeyController : ControllerBase
    {
        public readonly IDictKeyService _dictService;
        public readonly IDictValueService _dictValueService;

        /// <summary>
        /// 依赖注入
        /// </summary>
        /// <param name="dictService"></param>
        public DictKeyController(IDictKeyService dictService, IDictValueService dictValueService)
        {
            _dictService = dictService;
            _dictValueService = dictValueService;
        }

        /// <summary>
        /// 数据字典显示
        /// </summary>
        /// <returns></returns>
        [HttpGet("getList")]
        public IActionResult GetList()
        {
            //获取列表（不分页，无条件）
            var list = _dictService.GetList();

            return Ok(new ResponseModel<object>
            {
                Code = StatusCodes.Status200OK,
                Data = list,
                Msg = "操作成功"
            });
        }

        /// <summary>
        /// 数据字典添加
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Add(DictKeyAddUpdateDto input)
        {
            //模型验证
            if (!ModelState.IsValid)
            {
                return BadRequest(input); //返回400
            }

            int flag = _dictService.Insert(input);

            return Ok(new ResponseModel<int>
            {
                Code = StatusCodes.Status200OK,
                Data = flag,
                Msg = flag > 0 ? "添加成功" : "添加失败"
            });
        }

        /// <summary>
        /// 数据字典删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id == 0) BadRequest();

            //判断该字典下是否还有字典只
            if(_dictValueService.Query(id).Count > 0)
            {
                return Ok(new ResponseModel<int>
                {
                    Code = StatusCodes.Status200OK,
                    Data = -1,
                    Msg = "不能删除，该字典下还有字典项"
                });
            }

            int flag = _dictService.Delete(id);
            return Ok(new ResponseModel<int>
            {
                Code = StatusCodes.Status200OK,
                Data = flag,
                Msg = flag > 0 ? "删除成功" : "删除失败"
            });
        }

        /// <summary>
        /// 数据字典修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Update(DictKeyAddUpdateDto input)
        {
            //模型验证
            if (!ModelState.IsValid)
            {
                return BadRequest(input); //返回400
            }

            int flag = _dictService.Update(input);
            return Ok(new ResponseModel<int>
            {
                Code = StatusCodes.Status200OK,
                Data = flag,
                Msg = flag > 0 ? "修改成功" : "修改失败"
            });

        }
        /// <summary>
        /// 数据字典反填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getById/{id}")]
        ///反填
        public IActionResult Get(int id)
        {
            if (id == 0)
            {
                throw new ArgumentOutOfRangeException("无效的Id");
            }

            return Ok(new ResponseModel<object>
            {
                Code = StatusCodes.Status200OK,
                Data = _dictService.GetById(id),
                Msg = "操作成功"
            });
        }
    }
}
