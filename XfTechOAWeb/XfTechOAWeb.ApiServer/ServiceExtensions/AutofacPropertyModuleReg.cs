﻿
using Autofac;
using Microsoft.AspNetCore.Mvc;

namespace XfTechOAWeb.ApiServer.ServiceExtensions
{
    /// <summary>
    /// 分模块配置
    /// </summary>
    public class AutofacPropertyModuleReg : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //获取所有控制器类型并使用属性注入
            var controllerBaseType = typeof(ControllerBase);
            builder.RegisterAssemblyTypes(typeof(Program).Assembly)
                .Where(t => controllerBaseType.IsAssignableFrom(t) && t != controllerBaseType)
                .PropertiesAutowired();

        }
    }
}
