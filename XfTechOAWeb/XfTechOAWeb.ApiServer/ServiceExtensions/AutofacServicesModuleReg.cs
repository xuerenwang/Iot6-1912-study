﻿
using Autofac;
using Microsoft.AspNetCore.Mvc;
using XfTechOAWeb.EFCore.Base;
using XfTechOAWeb.EFCore;
using XfTechOAWeb.Infrastructure.Interceptors;
using XfTechOAWeb.Service;
using Autofac.Extras.DynamicProxy;
using System.Reflection;

namespace XfTechOAWeb.ApiServer.ServiceExtensions
{
    /// <summary>
    /// 分模块配置
    /// </summary>
    public class AutofacServicesModuleReg : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //注册拦截器到Autofac容器
            builder.RegisterType<ServiceLoggerInterceptor>();

            //AutoFac批量注册UserService所在程序集中所有Service类,并启用拦截器
            builder.RegisterAssemblyTypes(typeof(UserService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .PropertiesAutowired()
                .EnableInterfaceInterceptors()                    //这里动态创建一个接口代理，另外还有一个EnableClassInterceptors方法
                .InterceptedBy(typeof(ServiceLoggerInterceptor)); //动态注入拦截器ServiceLoggerInterceptor

            //AutoFac批量注册XfTechOAWeb.EFCore"中的所有Repository仓储类    
            builder.RegisterAssemblyTypes(Assembly.Load("XfTechOAWeb.EFCore"))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .PropertiesAutowired()
                .InstancePerLifetimeScope();  //域模式

            builder.RegisterType(typeof(TimingRepository)).AsImplementedInterfaces().SingleInstance();

            //AutoFac批量注册泛型仓储
            builder.RegisterGeneric(typeof(EfBaseRepository<>))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

        }
    }
}
