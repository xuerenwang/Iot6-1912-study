﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos.Dicts;
using XfTechOAWeb.EFCore;
using XfTechOAWeb.Models;
using Microsoft.EntityFrameworkCore;
using XfTechOAWeb.IService;
using XfTechOAWeb.Data.Base;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Service.Base;
using XfTechOAWeb.EFCore.Extensions;
using AutoMapper;

namespace XfTechOAWeb.Service
{
    /// <summary>
    /// 字典值 应用服务实现类
    /// </summary>
    public class DictValueService : BaseService<DictValue             //实体类型
                                               , int                 //主键类型
                                               , DictValueDto          //用于显示的Dto
                                               , DictValueAddUpdateDto //用与修改和新增的Dto 
                                               , PageRequestModel>   //用于分页的Dto
                                   , IDictValueService
    {
        private readonly IRepository<DictValue> _repositoryDictValue;
        private readonly IRepository<DictKey> _repositoryDictKey;
        private readonly IMapper _mapper;
        /// <summary>
        ///构造方法
        /// </summary>
        /// <param name="repositoryDict">仓储</param>
        public DictValueService(IMapper mapper,
                                IRepository<DictValue> repositoryDict,
                                IRepository<DictKey> repositoryDictKey) : base(repositoryDict)
        {
            _mapper = mapper;
            _repositoryDictValue = repositoryDict;
            _repositoryDictKey = repositoryDictKey;
        }

        /// <summary>
        /// 条件分页查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public PageResponseModel<DictValueDto> Query(DictValuePageRequestDto input)
        {
            //字典表，两表联查
            var query = from dv in _repositoryDictValue.Table
                        join dk in _repositoryDictKey.Table on dv.DictKeyId equals dk.DictKeyId
                        where (input.DictKeyId == 0 || dv.DictKeyId == input.DictKeyId)
                            && (string.IsNullOrEmpty(input.DictValueName) || dv.DictValueName.Contains(input.DictValueName))
                        select new DictValueDto
                        {
                            DictKeyId = dk.DictKeyId,
                            DictKeyName = dk.DictKeyName,
                            DictValueCode = dv.DictValueCode,
                            DictValueId = dv.DictValueId,
                            DictValueName = dv.DictValueName,
                            State = dv.State,
                            CreateTime = dv.CreateTime,
                            CreateUser = dv.CreateUser
                        };

            PageResponseModel<DictValueDto> result = new();
            result.TotalCount = query.Count();

            input.SortedBy = string.IsNullOrEmpty(input.SortedBy) ? "DictValueId" : input.SortedBy;
            if (input.SortType.Equals("ASC"))
            {
                query = query.OrderBy(input.SortedBy);
            }
            else
            {
                query = query.OrderByDescending(input.SortedBy);
            }

            result.PageList = query.Skip((input.PageIndex - 1) * input.PageSize)
                                   .Take(input.PageSize)
                                   .ToList() ;

            return result;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override int Update(DictValueAddUpdateDto input)
        {
            DictValue m = _repositoryDictValue.GetById(input.DictValueId);
            if (m != null)
            {
                m.State = input.State;
                m.DictValueCode = input.DictValueCode;
                m.DictValueName = input.DictValueName;
                m.UpdateTime = DateTime.Now;
                m.UpdateUser = CurrentUserName;

                _repositoryDictValue.Update(m);

                return _repositoryDictValue.SaveChanges();
            }

            return -1;
        }

        /// <summary>
        /// 根据字典键的主键，查询字典值
        /// </summary>
        /// <param name="dictKeyId"></param>
        /// <returns></returns>
        public List<DictValueDto> Query(int dictKeyId)
        {
            //字典表，两表联查
            var query = from dv in _repositoryDictValue.Table
                        where dv.DictKeyId == dictKeyId
                        select dv;

            return _mapper.Map<List<DictValueDto>>(query.ToList());
        }
    }
}
