﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos.MyMessages;

namespace XfTechOAWeb.Service.Hubs
{
    //强类型中心
    public interface IChatClient
    {
        Task ReceiveMessage(string user, string message);
        Task ReceiveMessage(MyMessageAddDto message);
    }
}
