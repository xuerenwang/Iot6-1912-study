﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Data;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Forms;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Service
{
    /// <summary>
    /// 表单设计实现服务
    /// </summary>
    public class FormDesignerService : IFormDesignerService
    {
        private readonly IFormDesignerRepository _formDesignerRepository;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="formDesignerRepository"></param>
        /// <param name="mapper"></param>
        /// <param name="httpContextAccessor"></param>
        public FormDesignerService(IFormDesignerRepository formDesignerRepository,
                                   IMapper mapper,
                                   IHttpContextAccessor httpContextAccessor)
        {
            _formDesignerRepository = formDesignerRepository;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 添加表单
        /// </summary>
        /// <param name="formDesigner"></param>
        /// <returns></returns>
        public int AddFormDesigner(FormDto input)
        {
            //获取当前登录用户
            var loginUser = _httpContextAccessor.HttpContext.User.FindFirst(x => x.Type == ClaimTypes.Name).Value;

            //实例化实体对象
            Form form = new Form();
            form.CreateName = loginUser;
            form.CreateTime = DateTime.Now;
            form.FormJson = input.FormJson.ToString();    /*jsons.Adapt<string>();*/
            form.FormName = input.FormName;
            form.Description = input.Description;    //表单描述

            return _formDesignerRepository.AddFormDesigner(form);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseModel<int> DeleteFormDesigner(string ids)
        {
            ResponseModel<int> result = new ResponseModel<int>();

            try
            {
                var h = _formDesignerRepository.DeleteFormDesigner(ids);
                if (h > 0)
                {
                    result.Code = 200;
                    result.Data = h;
                    result.Msg = "添加成功";
                }
                else
                {
                    result.Code = 500;
                    result.Data = h;
                    result.Msg = "添加失败";
                }
            }
            catch
            {

                result.Code = 500;
                result.Data = 0;
                result.Msg = "添加失败";
            }

            return result;
        }
        /// <summary>
        /// 显示
        /// </summary>
        /// <returns></returns>
        public ResponseModel<PageResponseModel<FormShowDto>> GetAllFormDesiSgner(string tableName, int pageIndex, int pageSize)
        {
            ResponseModel<PageResponseModel<FormShowDto>> result = new ResponseModel<PageResponseModel<FormShowDto>>();

            try
            {
                var list = _formDesignerRepository.GetAllFormDesiSgner();
                if (tableName != null)
                {
                    list = list.Where(x => x.FormName.Contains(tableName)).ToList();
                }
                var lists = _mapper.Map<List<FormShowDto>>(list);
                PageResponseModel<FormShowDto> page = new PageResponseModel<FormShowDto>()
                {
                    PageList = lists.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList(),
                    TotalCount = lists.Count,
                };

                result.Code = 200;
                result.Data = page;
                result.Msg = "请求成功";
            }
            catch
            {
                result.Code = 500;
                result.Data = null;
                result.Msg = "请求失败";
            }
            return result;
        }
        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Form GetFormDesigner(int id)
        {
            return _formDesignerRepository.GetFormDesigner(id);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="formDesigner"></param>
        /// <returns></returns>
        public ResponseModel<int> UpdateFormDesigner(FormDto input)
        {
            //获取实体对象
            Form form = _formDesignerRepository.Table.SingleOrDefault(x => x.Id == input.Id);
            form.UpdateTime = DateTime.Now;
            form.FormJson = input.FormJson.ToString();    /*jsons.Adapt<string>();*/
            form.FormName = input.FormName;
            form.Description = input.Description;    //表单描述

            int result = _formDesignerRepository.UpdateFormDesigner(form);

            var res = new ResponseModel<int>()
            {
                Code = 200,
                Data = result,
                Msg = result > 0 ? "更新成功" : "更新失败"
            };

            return res;
        }
    }
}
