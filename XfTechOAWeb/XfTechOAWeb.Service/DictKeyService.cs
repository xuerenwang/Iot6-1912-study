﻿using XfTechOAWeb.Dtos.Dicts;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;
using XfTechOAWeb.Data.Base;
using XfTechOAWeb.Service.Base;
using XfTechOAWeb.Dtos;
using static Dapper.SqlMapper;
using System;
using XfTechOAWeb.Models.Base;

namespace XfTechOAWeb.Service
{
    /// <summary>
    /// 字典模块应用服务实现类
    /// </summary>
    public class DictKeyService : BaseService<DictKey             //实体类型
                                            , int                 //主键类型
                                            , DictKeyDto          //用于显示的Dto
                                            , DictKeyAddUpdateDto //用与修改和新增的Dto 
                                            , PageRequestModel>   //用于分页的Dto
                                 , IDictKeyService
    {
        private readonly IRepository<DictKey> _repositoryDict;

        /// <summary>
        ///构造方法
        /// </summary>
        /// <param name="repositoryDict">仓储</param>
        public DictKeyService(IRepository<DictKey> repositoryDict) : base(repositoryDict)
        {
            _repositoryDict = repositoryDict;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override int Update(DictKeyAddUpdateDto input)
        {
            DictKey m = _repositoryDict.GetById(input.DictKeyId);
            if (m != null)
            {
                m.State = input.State;
                m.DictKeyCode = input.DictKeyCode;
                m.DictKeyName = input.DictKeyName;
                m.UpdateTime = DateTime.Now;
                m.UpdateUser = CurrentUserName;

                _repositoryDict.Update(m);

                return _repositoryDict.SaveChanges();
            }

            return -1;
        }


    }
}
