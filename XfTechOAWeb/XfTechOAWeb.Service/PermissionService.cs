﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.EFCore;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;
using AutoMapper;
using XfTechOAWeb.Data.Base;

namespace XfTechOAWeb.Service
{
    /// <summary>
    /// 权限
    /// </summary>
    public class PermissionService : IPermissionService
    {
        private readonly IRepository<Permission> _permissionRepository;
        private readonly  IMapper _mapper;
        public PermissionService(IRepository<Permission> permissionRepository, IMapper mapper)
        {
            _permissionRepository = permissionRepository;
            _mapper = mapper;
        }
        /// <summary>
        /// 获取所有的权限 （以层级结构的数据集合返回）
        /// </summary>
        /// <returns></returns>
        public List<PermissionTreeDto> GetPermissionTree()
        {
            var list = _permissionRepository.Table.ToList() ; //读出表中的所有权限数据

            List<PermissionTreeDto> data = GetTree(list);
            
            return data;
        }

        /// <summary>
        /// 递归方法 拼接 树
        /// </summary>
        /// <param name="permissions"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        private List<PermissionTreeDto> GetTree(List<Permission> permissions, int pid=0)
        {
            //查询父Id下面的所有数据
            var list = _permissionRepository.Table.Where(x => x.ParentId == pid).ToList();
            if (list.Count == 0)
            {
                return null;
            }
            
            List<PermissionTreeDto> tos = new List<PermissionTreeDto>();
            foreach (var item in list)
            {
                var node = new PermissionTreeDto()
                {
                    PId = item.PId,
                    PTitle = item.PTitle,
                    ParentId = item.ParentId,
                    PAction = item.PAction,
                    PApiUrl = item.PApiUrl,
                    IconName = item.IconName,
                    SortNo = item.SortNo,
                    IsMenu = item.IsMenu,
                    Children = GetTree(permissions, item.PId)
                };

                tos.Add(node);
            };

            return tos;
        }
        /// <summary>
        /// 权限添加
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public int Add(PermissionAddDto model)
        {
            //AutoMapper数据类型转换
            Permission permission= _mapper.Map<Permission>(model);
            _permissionRepository.Insert(permission);
            return _permissionRepository.SaveChanges();
        }

        /// <summary>
        /// 权限删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public int Del(string ids)
        {
            foreach (var item in ids.Split(','))
            {
                _permissionRepository.Remove(Convert.ToInt32(item));
            }
            return _permissionRepository.SaveChanges();
        }

        /// <summary>
        /// 权限显示
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<PermissionTreeDto> GetList()
        {
            var list = _permissionRepository.Table.ToList(); //读出表中的所有权限数据

            List<PermissionTreeDto> data = GetTree(list);

            return data;
        }

        /// <summary>
        /// 权限修改
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public int Upt(PermissionUpdDto model)
        {
            //转换成实体对象，更新并保存更改
            Permission permiss = _mapper.Map<Permission>(model);
            _permissionRepository.Update(permiss);
            return _permissionRepository.SaveChanges();
        }

        /// <summary>
        /// 根据主键获取权限
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Permission Get(int id)
        {
            return _permissionRepository.GetById(id);
        }

    }
}
