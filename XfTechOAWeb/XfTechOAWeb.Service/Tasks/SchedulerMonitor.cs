﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using XfTechOAWeb.Data;
using XfTechOAWeb.EFCore;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Service.Tasks
{
    public class SchedulerMonitor : ISchedulerListener
    {
        ITimingRepository timingRepository;
        int id = 0;
        public SchedulerMonitor(ITimingRepository timingRepository ) 
        {
            
            this.timingRepository = timingRepository;
            
        }
        #region 集中式方法
        /// <summary>
        /// 添加日志的操作
        /// </summary>
        /// <param name="jobDetail"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        private  bool FocusOn(string  msg,DateTime Time)
        {
            TimingLog Logs = new TimingLog();


            Logs.Msg = msg;
            Logs.StartTime = Time;
            var   a=  timingRepository.Insert(Logs);
            return a;

        }
      
        #endregion


        /// <summary>
        /// 新增Job
        /// </summary>
        /// <param name="jobDetail"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task ISchedulerListener.JobAdded(IJobDetail jobDetail, CancellationToken cancellationToken)
        {
            //TimingLog Logs = new TimingLog();
            //Logs.PGroupName = jobDetail.Key.ToString();
            //return Task.FromResult(FocusOn("JobAdded", DateTime));
            return Task.FromResult(true);
        }
        /// <summary>
        /// 结束Job
        /// </summary>
        /// <param name="jobKey"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task ISchedulerListener.JobDeleted(JobKey jobKey, CancellationToken cancellationToken)
        {

            return Task.FromResult(FocusOn("JobDeleted", DateTime.Now));
        }
        /// <summary>
        /// 暂停Job
        /// </summary>
        /// <param name="jobKey"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task ISchedulerListener.JobInterrupted(JobKey jobKey, CancellationToken cancellationToken)
        {

            return Task.FromResult(FocusOn("JobInterrupted", DateTime.Now));

        }

        Task ISchedulerListener.JobPaused(JobKey jobKey, CancellationToken cancellationToken)
        {

            return Task.FromResult(FocusOn("JobPaused", DateTime.Now));
        }

        Task ISchedulerListener.JobResumed(JobKey jobKey, CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("JobResumed", DateTime.Now));
        }

        Task ISchedulerListener.JobScheduled(ITrigger trigger, CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("JobScheduled", DateTime.Now));

        }

        Task ISchedulerListener.JobsPaused(string jobGroup, CancellationToken cancellationToken)
        {

            return Task.FromResult(FocusOn("JobsPaused", DateTime.Now));
        }

        Task ISchedulerListener.JobsResumed(string jobGroup, CancellationToken cancellationToken)
        {

            return Task.FromResult(FocusOn("JobsResumed", DateTime.Now));
        }

        Task ISchedulerListener.JobUnscheduled(TriggerKey triggerKey, CancellationToken cancellationToken)
        {

            return Task.FromResult(FocusOn("JobUnscheduled", DateTime.Now));
        }

        Task ISchedulerListener.SchedulerError(string msg, SchedulerException cause, CancellationToken cancellationToken)
        {

            return Task.FromResult(FocusOn("SchedulerError", DateTime.Now));
        }

        Task ISchedulerListener.SchedulerInStandbyMode(CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("SchedulerInStandbyMode", DateTime.Now));
        }

        Task ISchedulerListener.SchedulerShutdown(CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("JobDeleted", DateTime.Now));
        }

        Task ISchedulerListener.SchedulerShuttingdown(CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("SchedulerShuttingdown", DateTime.Now));
        }

        Task ISchedulerListener.SchedulerStarted(CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("SchedulerStarted", DateTime.Now));
        }

        Task ISchedulerListener.SchedulerStarting(CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("SchedulerStarting", DateTime.Now));
        }

        Task ISchedulerListener.SchedulingDataCleared(CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("SchedulingDataCleared", DateTime.Now));
        }

        Task ISchedulerListener.TriggerFinalized(ITrigger trigger, CancellationToken cancellationToken)
        {
            Task.FromResult(timingRepository.Finish(trigger.Key.ToString()));//释放Job的时候要将其状态还原,避免显示和数据不符
            return  Task.FromResult(FocusOn("TriggerFinalized", DateTime.Now));

        }

        Task ISchedulerListener.TriggerPaused(TriggerKey triggerKey, CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("TriggerPaused", DateTime.Now));
        }

        Task ISchedulerListener.TriggerResumed(TriggerKey triggerKey, CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("TriggerResumed", DateTime.Now));
        }

        Task ISchedulerListener.TriggersPaused(string triggerGroup, CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("TriggersPaused", DateTime.Now));
        }

        Task ISchedulerListener.TriggersResumed(string triggerGroup, CancellationToken cancellationToken)
        {
            return Task.FromResult(FocusOn("TriggersResumed", DateTime.Now));
        }
    }
}
