﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Service.Tasks
{
    public class two : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(() =>
            {
                //  context.JobDetail.JobDataMap.Get("字段名");获取JobDetail传入的值
                //  context.Trigger.JobDataMap.Get("字段名");//获取trigger传入的值
                //  context.MergedJobDataMap.Get("字段名")//获取键值Key相同中的后者值
                Console.WriteLine("+++++++++++++++++++++++++=");
                Console.WriteLine(" ");
                Console.WriteLine("嘿嘿嘿，我每五秒要吃一顿饭");
                Console.WriteLine(" ");
                Console.WriteLine("+++++++++++++++++++++++++=");
            });
        }
    }
}
