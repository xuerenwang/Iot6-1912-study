﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using XfTechOAWeb.Data;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Service.Tasks
{
    /// <summary>
    /// Job监听器
    /// 在job执行之前，之后或者执行过程中动态去添加一些额外的动作
    /// </summary>
    public class JobMonitor : IJobListener
    {
        public string Name { get; } = "JobLogs";

        ITimingRepository timingRepository;
        TimingLog Logs = new TimingLog();
        public JobMonitor(ITimingRepository repository)
        {
            this.timingRepository = repository;
        }
        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            Logs.Msg = "JobExecutionVetoed";
            Logs.StartTime = DateTime.Now;
            return Task.FromResult(timingRepository.Insert(Logs));
        }

        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            Logs.Msg = "JobExecutionVetoed";
            Logs.StartTime = DateTime.Now;
            return Task.FromResult(timingRepository.Insert(Logs));


        }

        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default)
        {
            Logs.Msg = "JobExecutionVetoed";
            Logs.StartTime = DateTime.Now;
            return Task.FromResult(timingRepository.Insert(Logs));
        }
    }
}
