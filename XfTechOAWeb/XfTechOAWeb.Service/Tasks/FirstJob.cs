﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Service.Tasks
{
    //新建一个IJob的实现类
    public class FirstJob : IJob
    {
        /// <summary>
        /// 执行具体任务
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                System.Console.WriteLine("我的第一个定时任务");
            });
        }
    }
}
