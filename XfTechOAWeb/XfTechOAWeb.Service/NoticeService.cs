﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using XfTechOAWeb.Data.Base;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Notices;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;
using XfTechOAWeb.Service.Hubs;

namespace XfTechOAWeb.Service
{
    /// <summary>
    /// 消息通知服务实现
    /// </summary>
    public class NoticeService : INoticeService
    {
        //公告表的仓储
        private readonly IRepository<Notice> _noticeRepository;
        //实时通知
        private readonly IHubContext<MessageHub> _hubContext;
        //HttpContext访问
        private readonly IHttpContextAccessor _httpContextAccessor;

        //构造方法
        public NoticeService(IRepository<Notice> notice, 
                             IHubContext<MessageHub> hub, 
                             IHttpContextAccessor httpContextAccessor)
        {
            _noticeRepository = notice;
            _hubContext = hub;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 添加通知
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public int AddNotice(NoticeAddDto dto)
        {
            //创建公告对象
            Notice notice = new();
            notice.NoticeTitle = dto.NoticeTitle;
            notice.NoticeContent = dto.NoticeContent;
            notice.Status = NoticeStatus.正常;
            notice.CreatedTime = DateTime.Now;
            notice.UserType = NoticeUserType.发送给所有人;
            notice.CreatedUserID = Convert.ToInt32(_httpContextAccessor.HttpContext.User.FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value ?? "0") ;
            notice.CreatedUser= _httpContextAccessor.HttpContext.User.FindFirst(x => x.Type == ClaimTypes.Name).Value ?? "系统管理员";

            //添加到数据库
            _noticeRepository.Insert(notice);
            int result = _noticeRepository.SaveChanges();

            //用SignalR实时通知给客户端
            if(result > 0)
            {
                _hubContext.Clients.All.SendAsync("ReceiveMessage", notice.NoticeTitle);
            }

            return result;
        }

        /// <summary>
        /// 通知公告列表
        /// </summary>
        /// <param name="pageDto"></param>
        /// <returns></returns>
        public PageResponseModel<NoticeListDto> GetNoticeByPage(NoticePageDto pageDto)
        {
            var query = from s in _noticeRepository.Table.AsNoTracking()
                        where string.IsNullOrEmpty(pageDto.NoticeTitle) || s.NoticeTitle.StartsWith(pageDto.NoticeTitle)
                        orderby s.CreatedTime descending
                        select new NoticeListDto
                        {
                            NoticeId = s.NoticeId,
                            NoticeTitle = s.NoticeTitle,
                            NoticeContent = s.NoticeContent,
                            Status = s.Status,
                            CreatedTime = s.CreatedTime,
                            CreatedUserID = s.CreatedUserID,
                            CreatedUser = s.CreatedUser,
                            UserType= s.UserType
                        };

            PageResponseModel<NoticeListDto> page = new PageResponseModel<NoticeListDto>()
            {
                TotalCount = query.Count(),
                PageList = query.Skip((pageDto.PageIndex - 1) * pageDto.PageSize)
                                .Take(pageDto.PageSize)
                                .ToList()
            };
            return page;
        }

        /// <summary>
        /// 获取id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Notice GetNoticeId(int noticeId)
        {
            var data = _noticeRepository.GetById(noticeId);
            return data;
        }
    }
}
