﻿#region 文件说明
/*
 * *******************************************************************
 * 
 * 版权：MightyMan有限公司
 * 
 * *******************************************************************
 * 
 * 创建人：周华岳
 * 创建时间：2022/7/28
 * 
 * *******************************************************************
 * 
 * 功能描述：系统日志处理服务
 * 
 * *******************************************************************
 * 修改履历：
 * 周先生 + 系统日志处理服务
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Data.Base;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Syslog;
using XfTechOAWeb.IService;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Service
{
    /// <summary>
    /// 操作日志服务
    /// </summary>
    public class OperationLogService : IOperationLogService
    {
        private readonly IRepository<OperationLog> _logRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logRepository"></param>
        public OperationLogService(IRepository<OperationLog> logRepository)
        {
            _logRepository = logRepository;
        }

        /// <summary>
        /// 添加系统日志
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        public int Add(OperationLog operation)
        {
            operation.OperatorName = operation.OperatorName.Trim();
            _logRepository.Insert(operation);
            return _logRepository.SaveChanges();
        }

        /// <summary>
        /// 根据条件获取所有数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<OperationLog> GetList(LogsPageRequestDto input)
        {
            var query = from l in _logRepository.Table
                        where (string.IsNullOrEmpty(input.OperatorName) || l.OperatorName.Contains(input.OperatorName))
                        && l.Classify.Equals(input.Classify) //根据分类名查询
                        orderby l.CreateTimes descending
                        select l;
            return query.ToList();
        }

        /// <summary>
        /// 分页显示
        /// </summary>
        /// <param name="dTO"></param>
        /// <returns></returns>
        public PageResponseModel<OperationLog> GetPageList(LogsPageRequestDto input)
        {
            var query = from l in  _logRepository.Table
                        where (string.IsNullOrEmpty(input.OperatorName) || l.OperatorName.Contains(input.OperatorName))
                        where l.Classify.Equals(input.Classify) //根据分类名查询
                        orderby l.CreateTimes descending
                        select l;

            PageResponseModel<OperationLog> result = new PageResponseModel<OperationLog>
            {
                PageList = query.Skip((input.PageIndex - 1) * input.PageSize).Take(input.PageSize).ToList(),
                TotalCount = query.Count()
            };

            return result;
        }
    }
}
