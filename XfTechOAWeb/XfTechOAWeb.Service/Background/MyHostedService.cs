using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using XfTechOAWeb.IService;

namespace XfTechOAWeb.Service.Background
{
    /// <summary>
    /// 自定义定时服务
    /// </summary>
    public class MyHostedService : BackgroundService
    {
        private Timer _timer;
        /// <summary>
        /// 执行任务
        /// </summary>
        /// <param name="stoppingToken"></param>
        /// <returns></returns>
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //如果没停止，就一直执行
            int i = 1;

            //while (!stoppingToken.IsCancellationRequested)
            //{
                DoWork(i++);
            //}
            //_timer = new Timer(DoWork, i++, TimeSpan.Zero, TimeSpan.FromSeconds(2));


            return Task.CompletedTask;
        }

        /// <summary>
        /// 打印输出一句话
        /// </summary>
        /// <param name="i"></param>
        private void DoWork(object? p)
        {
            Console.WriteLine("后台任务执行MyHostedService，第{0}次", p);
        }
    }
}