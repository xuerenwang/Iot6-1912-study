﻿namespace XfTechOAWeb.Dtos.Notices
{
    /// <summary>
    /// 接收发送消息
    /// </summary>
    public class NoticeSendDto
    {
        /// <summary>
        /// 主键id
        /// </summary>
        public int NoticeId { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string NoticeContent { get; set; }
    }
}
