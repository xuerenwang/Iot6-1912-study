﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Dtos.Notices
{
    /// <summary>
    /// 通知Dto
    /// </summary>
    public class NoticeListDto
    {
        /// <summary>
        /// 主键id
        /// </summary>
        public int NoticeId { get; set; }
        
        /// <summary>
        /// 标题
        /// </summary>
        public string NoticeTitle { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public NoticeUserType UserType { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string NoticeContent { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public NoticeStatus Status { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public int CreatedUserID { get; set; }

        /// <summary>
        /// 发布人名称
        /// </summary>
        public string CreatedUser { get; set; }
    }
}
