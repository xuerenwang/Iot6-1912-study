﻿using System.ComponentModel.DataAnnotations;

namespace XfTechOAWeb.Dtos.Notices
{
    //添加公告
    public class NoticeAddDto
    {
        /// <summary>
        /// 标题
        /// </summary>
        [Required]
        [StringLength(50)]
        public string NoticeTitle { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Required]
        public string NoticeContent { get; set; }

        //todo:发送给的 用户类型
    }
}
