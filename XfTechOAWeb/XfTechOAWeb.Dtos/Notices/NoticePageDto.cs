﻿
namespace XfTechOAWeb.Dtos.Notices
{
    /// <summary>
    /// 分页查询dto类
    /// </summary>
    public class NoticePageDto : PageRequestModel
    {
        /// <summary>
        /// 通知标题
        /// </summary>
        public string NoticeTitle { get; set; }
    }
}
