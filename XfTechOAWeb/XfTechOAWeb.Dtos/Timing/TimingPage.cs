﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Timing
{
   public class TimingPage
    {
        public int Pindex { get; set; }
        public int Psize { get; set; }

    }
}
