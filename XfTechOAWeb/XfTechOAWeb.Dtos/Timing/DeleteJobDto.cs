﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Timing
{
    /// <summary>
    /// 暂停JobDto
    /// </summary>
   public class DeleteJobDto
    {
        /// <summary>
        /// Job名称
        /// </summary>
        public string JobName { get; set; }
        /// <summary>
        /// Job分区
        /// </summary>
        public string JobGroup { get; set; }
        public int JobId { get; set; }
    }
}
