﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Timing
{
   public class TimingDto
    {
        public int Id { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string JobName { get; set; }
        /// <summary>
        /// 任务分组
        /// </summary>
        public string JobGroup { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTimeOffset BeginTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTimeOffset? EndTime { get; set; }
        /// <summary>
        /// Cron字符串
        /// </summary>
        public string Cron { get; set; }
        /// <summary>
        /// 执行次数
        /// </summary>
        public int? RumTimes { get; set; }

        /// <summary>
        /// 执行间隔时间，单位秒(如果使用的是Cron,则失效)
        /// </summary>
        public int? IntervalSecond { get; set; }

        /// <summary>
        /// 触发器类型
        /// </summary>
        public string TriggerType { get; set; }
        /// <summary>
        /// 请求Url
        /// </summary>
        public string RequestUrl { get; set; }
        /// <summary>
        /// 命名名称
        /// </summary>
        public string Namespace { get; set; }
        /// <summary>
        /// 是否持久化
        /// </summary>
        public bool Yun { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Job任务的状态
        /// </summary>
        public int Zt { get; set; } = 0;//o禁用,1启用中
    }
}
