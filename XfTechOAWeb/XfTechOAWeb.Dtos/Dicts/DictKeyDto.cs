﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Dicts
{
    /// <summary>
    /// 字典显示DTO
    /// </summary>
    public class DictKeyDto
    {
        /// <summary>
        /// 字典Id
        /// </summary>
        public int DictKeyId { get; set; }

        /// <summary>
        /// 字典名称
        /// </summary>
        public string DictKeyName { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 字典代码
        /// </summary>
        public string DictKeyCode { get; set; }
    }
}
