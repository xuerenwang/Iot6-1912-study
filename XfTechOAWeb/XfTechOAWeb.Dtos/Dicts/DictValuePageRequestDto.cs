﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Dicts
{
    /// <summary>
    /// 分页查询Dto
    /// </summary>
    public class DictValuePageRequestDto : PageRequestModel
    {
        /// <summary>
        /// 字典Id
        /// </summary>
        public int DictKeyId { get; set; }

        /// <summary>
        /// 字典名称
        /// </summary>
        public string DictValueName { get; set; }


    }
}
