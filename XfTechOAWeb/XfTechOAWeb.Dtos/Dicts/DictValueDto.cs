﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Dicts
{
    /// <summary>
    /// 字典显示DTO
    /// </summary>
    public class DictValueDto
    {
        public int DictValueId { get; set; }

        public string DictValueName { get; set; }

        public int DictKeyId { get; set; }

        public string DictKeyName { get; set; }

        public int State { get; set; }

        public string DictValueCode { get; set; }

        public string CreateUser { get; set; }

        public DateTime? CreateTime { get; set; }
    }
}
