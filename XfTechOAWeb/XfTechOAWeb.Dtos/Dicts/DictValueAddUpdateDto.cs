﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Dicts
{
    /// <summary>
    /// 添加/修改字典输入DTO
    /// </summary>
    public class DictValueAddUpdateDto
    {
        /// <summary>
        /// 字典Id
        /// </summary>
        public int DictValueId { get; set; }

        public int DictKeyId { get; set; }

        /// <summary>
        /// 字典名称
        /// </summary>
        [StringLength(50)]
        [Required]
        public string DictValueName { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int State { get; set; } = 1; //0：禁用，1：启用

        /// <summary>
        /// 字典代码
        /// </summary>
        [StringLength(30)]
        [Required]
        public string DictValueCode { get; set; }
    }
}
