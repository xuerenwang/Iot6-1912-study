﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos
{
    /// <summary>
    /// 分页结果集  泛型类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageResponseModel<T>
    {
        public int TotalCount { get; set; }  //总条数

        public List<T> PageList { get; set; } //结果集
    }
}
