﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Roles
{
    /// <summary>
    /// 角色添加模型
    /// </summary>
    public class RoleAddDto
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        [StringLength(30)]
        [Required]
        public string RName { get; set; }
        /// <summary>
        /// 角色描述
        /// </summary>
        [StringLength(100)]
        [Required]
        public string RText { get; set; }
        /// <summary>
        /// 状态 0：禁用、 1： 启用
        /// </summary>
        public int Status { get; set; } = 1;
    }
}
