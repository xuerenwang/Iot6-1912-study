﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Roles
{
    public class RoleFindPageDto
    {
        public int pageIndex { get; set; } = 1;
        public int pageSize { get; set; } = 10;
        [StringLength(10)]
        public string name { get; set; }=null;
    }
}
