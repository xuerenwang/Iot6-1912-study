﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Roles
{
    /// <summary>
    /// 给角色添加权限
    /// </summary>
    public class RoleAddPermissionDto
    {
        public int RId { get; set; }
        public string PId { get; set; }  // 或 List<int> PId
    }
}
