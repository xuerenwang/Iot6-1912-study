﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Forms
{
    /// <summary>
    /// 创建或更新表单的Dto
    /// </summary>
    public class FormDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 表单名称
        /// </summary>
        public string FormName { get; set; }

        /// <summary>
        /// 表单JSON对象
        /// </summary>
        public object FormJson { get; set; }

        /// <summary>
        /// 表单描述
        /// </summary>
        public string Description { get; set; }
    }
}
