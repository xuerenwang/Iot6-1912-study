﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.MyMessages
{
    /// <summary>
    /// 消息dto信息
    /// </summary>
    public class MyMessageListDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int MId { get; set; }

        /// <summary>
        /// 公告Id
        /// </summary>
        public int NoticeId { get; set; }


        /// <summary>
        /// 状态,已读或者未读(0未读1已读)
        /// </summary>
        public int MessageState { get; set; }

        /// <summary>
        /// 消息创建时间
        /// </summary>
        public DateTime MessageDate { get; set; }

        //标题
        public string NoticeTitle { get; set; }

        //消息内容
        public string NoticeContent { get; set; }

        //发布人
        public string CreatedUser { get; set; }

    }
}
