﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.MyMessages
{
    /// <summary>
    /// 分页查询dto类，dto是数据传输对象
    /// </summary>
    public class MyMessagePageDto : PageRequestModel
    {
        /// <summary>
        /// 消息内容查询
        /// </summary>
        [StringLength(50)]
        public string Content { get; set; }
        /// <summary>
        /// 消息状态
        /// </summary>
        public int? Status { get; set; }
    }
}
