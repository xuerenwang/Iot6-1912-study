﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.MyMessages
{
    /// <summary>
    /// 添加消息dto
    /// </summary>
    public class MyMessageAddDto
    {
        public int UserId { get; set; }

        public int Id { get; set; }
        /// <summary>
        /// 消息发件人
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Sender { get; set; }
        /// <summary>
        /// 消息分类如系统消息
        /// </summary>
        [Required]
        [StringLength(20)]
        public string MessageType { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Receiver { get; set; }
        /// <summary>
        /// <summary>
        /// 消息内容
        /// </summary>
        [Required]
        [StringLength(400)]
        public string MessageContent { get; set; }
    }
}
