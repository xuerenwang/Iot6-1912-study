﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.MyMessages
{
    public class MyMessageUpdateDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public int MId { get; set; }
        /// <summary>
        /// 消息分类如系统消息
        /// </summary>
        [Required]
        [StringLength(20)]
        public string MessageType { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Receiver { get; set; }
        /// <summary>
        /// 状态,已读或者未读(0未读1已读)
        /// </summary>
        public int MessageState { get; set; }
        /// <summary>
        /// 消息内容
        /// </summary>
        [Required]
        [StringLength(400)]
        public string MessageContent { get; set; }
    }
}
