﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos
{
    /// <summary>
    /// 权限列表分页请求
    /// </summary>
    public class PermissionPageRequestDto : PageRequestModel
    {
        [StringLength(50)]
        public string Name { get; set; } //权限名称查询
    }
}
