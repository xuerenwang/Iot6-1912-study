﻿namespace XfTechOAWeb.Dtos.UserMsg
{
    public class DelDto
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户消息id
        /// </summary>
        public int UserMsgId { get; set; }
    }
}
