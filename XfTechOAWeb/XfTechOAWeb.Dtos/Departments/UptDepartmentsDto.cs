﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Departments
{
    /// <summary>
    /// 添加部门DTO
    /// </summary>
    public class UptDepartmentsDto
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public int DepId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        [StringLength(20)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 上级部门Id
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 层级Id
        /// </summary>
        public string CascadeId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
    }
}
