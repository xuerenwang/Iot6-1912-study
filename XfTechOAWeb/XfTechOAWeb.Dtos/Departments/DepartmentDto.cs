﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Departments
{
    /// <summary>
    /// 部门显示DTO
    /// </summary>
    public class DepartmentDto
    {
        /// <summary>
        /// 部门Id
        /// </summary>
        public int DepId { get; set; }
        /// <summary>
        /// 部门用户Id
        /// </summary>
        public int DUId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        [StringLength(20)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 用户账号
        /// </summary>
        [StringLength(50)]
        [Required]
        public string Account { get; set; }

        /// <summary>
        /// 上级部门Id
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 状态 0：禁用、 1： 启用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int IsChecks { get; set; }
        /// <summary>
        /// 层级Id
        /// </summary>
        [Required]
        [StringLength(20)]
        public string CascadeId { get; set; }
        /// <summary>
        /// 上级部门名称
        /// </summary>
        [Required]
        [StringLength(20)]
        public string PName { get; set; }
    }
}
