﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Departments
{
    /// <summary>
    /// 添加部门DTO
    /// </summary>
    public class AddDepartmentsDto
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        [StringLength(20)]   //特性注解：对输入的参数做一个限制
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 上级部门Id
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 部门状态
        /// </summary>
        public int Status { get; set; }
  
    }
}
