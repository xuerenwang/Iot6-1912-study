﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Dtos.Departments
{
    /// <summary>
    /// 部门分页DTO
    /// </summary>
    public class DepartmentPageDto
    {
        /// <summary>
        /// 页码
        /// </summary>
        public int Pindex { get; set; } = 1;
        /// <summary>
        /// 页大小
        /// </summary>
        public int Psize { get; set; } = 10;
        /// <summary>
        /// 层级ID
        /// </summary>
        public string did { get; set; } 
        /// <summary>
        /// 部门名称查询
        /// </summary>
        public string names { get; set; }
    }
}
