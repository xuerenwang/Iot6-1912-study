﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Dtos.Departments
{
    /// <summary>
    /// 返填DTO
    /// </summary>
    public class BackDepartmentDto
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public int DepId { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string uId { get; set; }
        /// <summary>
        /// 规则
        /// </summary>
        public string roles { get; set; }
        [StringLength(50)]
        [Required]
        public string Account { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; }
        /// <summary>
        /// 状态 0：禁用、 1： 启用
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        [StringLength(50)]
        [Required]
        public string UName { get; set; }
    }
}
