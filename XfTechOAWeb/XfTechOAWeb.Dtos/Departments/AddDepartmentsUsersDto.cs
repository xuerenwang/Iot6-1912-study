﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Departments
{
    /// <summary>
    /// 部门添加用户DTO
    /// </summary>
    public class AddDepartmentsUsersDto
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public int DepId { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string UIds { get; set; }
    }
}
