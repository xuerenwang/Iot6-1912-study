﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.Syslog
{
    /// <summary>
    /// 系统日志分页查询
    /// </summary>
   public class LogsPageRequestDto: PageRequestModel
    {
        /// <summary>
        /// 根据操作人模糊查询
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// 日志分类
        /// </summary>
        public string Classify { get; set; }

    }
}
