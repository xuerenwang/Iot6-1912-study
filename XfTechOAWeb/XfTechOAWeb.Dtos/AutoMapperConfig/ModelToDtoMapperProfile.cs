﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using XfTechOAWeb.Dtos.Dicts;
using XfTechOAWeb.Dtos.Forms;
using XfTechOAWeb.Dtos.TodoItems;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.Dtos.AutoMapperConfig
{
    //实体模型 转换为DTO的Mapper配置类
    public class ModelToDtoMapperProfile : Profile
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public ModelToDtoMapperProfile()
        {
            CreateMap<User, UserDto>(); // User转换为UserDto
            CreateMap<UserDto, User>(); // UserDto转换为User

            //以上两句可以用以下一句来代替
            //CreateMap<UserDto, User>().ReverseMap();
            CreateMap<PermissionAddDto, Permission>();
            CreateMap<PermissionUpdDto, Permission>();

            CreateMap<Form,FormShowDto>();
            CreateMap<CreateUpdateTodoItemDto, TodoItem>();

            //字典管理
            CreateMap<DictKey, DictKeyDto>();
            CreateMap<DictKeyAddUpdateDto, DictKey>();

            CreateMap<DictValue, DictValueDto>();
            CreateMap<DictValueAddUpdateDto, DictValue>();


        }
    }
}
