﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos
{
    public class UserUptDto
    {
        public int Uid { get; set; }//用户主键
        public string UName { get; set; }//用户姓名
        public string Password { get; set; }//用户密码
        public bool Status { get; set; }//启用禁用
    }
}
