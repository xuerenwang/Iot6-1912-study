﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos
{
    /// <summary>
    /// 用户分页请求
    /// </summary>
    public class UserPageRequestDto : PageRequestModel
    {
        [StringLength(50)]
        public string Name { get; set; } //用户名查询

        public int? DepId { get; set; }  //部门查询

        public bool? Status { get; set; } //状态查询

        public DateTime? CreateTimeFrom { get; set; } //创建时间 起始
        public DateTime? CreateTimeTo { get; set; } //创建时间 截止
    }
}
