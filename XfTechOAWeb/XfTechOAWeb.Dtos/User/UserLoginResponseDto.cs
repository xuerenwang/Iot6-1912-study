﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Infrastructure.JWT;

namespace XfTechOAWeb.Dtos
{
    /// <summary>
    /// 返回信息模型类
    /// </summary>
    public class UserLoginResponseDto : ResponseModel<object>
    {
        /// <summary>
        /// Token信息
        /// </summary>
        public TokenResult TokenInfo { get; set; }

        /// <summary>
        /// 刷新用的Token
        /// </summary>
        public string RefreshToken { get; set; }
    }
}
