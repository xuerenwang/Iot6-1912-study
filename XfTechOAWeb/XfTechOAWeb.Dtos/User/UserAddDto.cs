﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos
{
    /// <summary>
    /// 添加用户DTO
    /// </summary>
    public class UserAddDto
    {
        [StringLength(50)]
        [Required]
        public string Account { get; set; }

        [StringLength(50,MinimumLength = 5)]
        [Required]
        public string Password { get; set; }

        [StringLength(50)]
        [Required]
        public string UName { get; set; }
        public bool Status { get; set; }
        public bool IsDeleted { get; set; } = false;


    }
}
