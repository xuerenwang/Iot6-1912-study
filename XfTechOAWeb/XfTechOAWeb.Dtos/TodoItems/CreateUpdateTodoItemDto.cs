﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Dtos.TodoItems
{
    public class CreateUpdateTodoItemDto
    {
        public int ItemId { get; set; }

        /// <summary>
        /// 待办事项
        /// </summary>
        [StringLength(100)]
        [Required]
        public string Text { get; set; }

        public bool Status { get; set; } 

    }
}
