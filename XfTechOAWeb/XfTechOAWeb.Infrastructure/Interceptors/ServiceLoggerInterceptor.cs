﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.DynamicProxy;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace XfTechOAWeb.Infrastructure.Interceptors
{
    /// <summary>
    /// 此拦截器用户输出Service层各方法的性能日志
    /// </summary>
    public class ServiceLoggerInterceptor : IInterceptor
    {
        private readonly ILogger<ServiceLoggerInterceptor> _logger;
        
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="logger"></param>
        public ServiceLoggerInterceptor(ILogger<ServiceLoggerInterceptor> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 拦截器接口方法
        /// </summary>
        /// <param name="invocation"></param>
        public void Intercept(IInvocation invocation)
        {
            //执行方法前 做点什么
            //开始秒表技术
            var sw = new Stopwatch();
            sw.Start();
            //执行方法
            invocation.Proceed();

            //执行方法后 做点什么
            //结束秒表计时
            sw.Stop();

            //输出日志
            string actionName = invocation.Method.Name;
            string className = invocation.TargetType.Name;
            _logger.LogInformation($"执行了{className}的{actionName}方法，用时{sw.ElapsedMilliseconds}毫秒\n,执行结果是{invocation.ReturnValue}");
        }
    }
}
