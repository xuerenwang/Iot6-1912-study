
using Microsoft.Extensions.DependencyInjection;
using CSRedis;

namespace XfTechOAWeb.Infrastructure.RedisCache
{
    /// <summary>
    /// 此扩展方法用于注册CSReidsHelper的服务
    /// </summary>
    public static class CSRedisServiceCollectionExtensions
    {
        public static IServiceCollection AddCSRedisCache(this IServiceCollection services, string redisConnectionStr)
        {
            //注册CSRedisHelper服务
            services.AddScoped<ICSRedisHelper, CSRedisHelper>();  

            var csredis = new CSRedisClient(redisConnectionStr);
            RedisHelper.Initialization(csredis); //初始化ReidsHelper

            return services;
        }
    }
}