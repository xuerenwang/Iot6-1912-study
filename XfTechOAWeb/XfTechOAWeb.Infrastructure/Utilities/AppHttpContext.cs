﻿using Microsoft.AspNetCore.Http;

namespace XfTechOAWeb.Infrastructure.Utilities
{
    /// <summary>
    /// HttpContext公共类
    /// </summary>
    public class AppHttpContext
    {
        //私有字段
        private static IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// 配置服务
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

        }

        /// <summary>
        /// 获取当前Http上下文
        /// </summary>
        public static HttpContext Current
        {
            get
            {
                return _httpContextAccessor.HttpContext;
            }
        }
    }
}
