﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Infrastructure.Utilities
{
    /// <summary>
    /// 读出appsetting.json配置文件
    /// </summary>
    public class AppConfigurtaionServices
    {
        public static IConfiguration Configuration { get; set; }

        /// <summary>
        /// 静态构造方法
        /// </summary>
        static AppConfigurtaionServices()
        {
            //获取环境变量
            string env = System.Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            //读取配置文件
            Configuration = new ConfigurationBuilder()
                           .Add(new JsonConfigurationSource { 
                               Path = $"appsettings.{env}.json", 
                               ReloadOnChange = true 
                           })
                           .Build();
        }

        /// <summary>
        /// 获取json配置中的节点信息
        /// </summary>
        /// <param name="sections"></param>
        /// <returns></returns>
        public static string Get(params string[] sections)
        {
            try
            {

                if (sections.Any())
                {
                    return Configuration[string.Join(":", sections)];
                }
            }
            catch (Exception) { }

            return "";
        }
    }
}
