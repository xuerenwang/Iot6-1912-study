﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XfTechOAWeb.Infrastructure.Extensions
{
    public static class StringExtentions
    {
        public static bool IsNotEmptyOrNull(this string str)
        {
            return string.IsNullOrEmpty(str) ? false : true;
        }
    }
}
