﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XfTechOAWeb.Infrastructure.Filters
{
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Method,AllowMultiple =true)]
    public class NoLogAttribute : Attribute
    {

    }
}
