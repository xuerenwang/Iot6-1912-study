﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.MyMessages;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.IService
{
    public interface IMyMessageService
    {
        /// <summary>
        /// 获取id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        MyMessage GetMessageId(int id);

        /// <summary>
        /// 分页显示
        /// </summary>
        /// <param name="pageDto"></param>
        /// <returns></returns>
        PageResponseModel<MyMessageListDto> GetMessageByPage(MyMessagePageDto pageDto);

        /// <summary>
        /// 修改成已读
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int UpdateStatus(int id, int status);

        /// <summary>
        /// 获取未读数量
        /// </summary>
        /// <returns></returns>
        int GetUnReadCount();
        
    }
}
