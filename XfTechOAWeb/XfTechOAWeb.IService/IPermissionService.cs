﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Models;


namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 权限服务接口
    /// </summary>
    public interface IPermissionService
    {
        int Add(PermissionAddDto permission);

        int Upt(PermissionUpdDto permission);

        int Del(string ids);
        Permission Get(int id);

        List<PermissionTreeDto> GetList();  //列表显示

        List<PermissionTreeDto> GetPermissionTree();    //树形列表显示
    }
}
