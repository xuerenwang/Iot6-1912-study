﻿using XfTechOAWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Roles;

namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 角色管理功能模块--功能用例如下
    /// </summary>
    public interface IRoleService
    {
        int Add(RoleAddDto role);

        int Upt(Role role);

        int Del(string ids);

        PageResponseModel<Role> Show(RoleFindPageDto findPage);

        //List<PermissionTreeDto> PTree(int pid);

        int AddRolePermission(int rid, string pid);

        List<int> GetRolePermission(int rid);

        ResponseModel<string> UniqueRName(string RName);

        //获取该角色下有哪些用户ID
        List<int> GetRoleUser(int rid);
    }
}
