﻿using XfTechOAWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;

namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 用户管理功能模块--功能用例如下
    /// </summary>
    public interface IUserService
    {
        int Add(UserAddDto user); //用户添加
        int Upt(UserUptDto user);///用户修改
        int Del(string ids);//用户删除
        PageResponseModel<UserDto> GetList(UserPageRequestDto input); 
        User Get(int id);//用户反填
        UserDto GetUserByNameAndPwd(UserLoginRequestDto input);
        int AddUserRole(UserRoleAddDto  urole);//分配角色
        List<PermissionTreeDto> GetUserMenu(int userId); //获取用户菜单树 数据
        List<Permission> GetUserPermissions(int userId); //根据用户主键获取他的所有权限
        void GetUserCount(); //获取用户数并存放到缓存中
        int PasswordUpt(UserPassUpt password);//获取主键查询修改密码
        User UserGet(int userid);//用户详细信息

    }
}
