﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Dicts;

namespace XfTechOAWeb.IService.Base
{
    /// <summary>
    /// 应用服务基接口（单实体CRUD）
    /// </summary>
    /// <typeparam name="Tkey">主键类型</typeparam>
    /// <typeparam name="TEntityDto">用户显示的输出用Dto</typeparam>
    /// <typeparam name="TAddUpdateDto">添加和修改的输入用DTO</typeparam>
    /// <typeparam name="TPagingDto">分页DTO</typeparam>
    public interface IBaseService<Tkey, TEntityDto, TAddUpdateDto, TPagingDto> 
                where Tkey : struct
                where TEntityDto:class
                where TAddUpdateDto : class
                where TPagingDto : PageRequestModel
    {
        /// <summary>
        /// 显示
        /// </summary>
        /// <returns></returns>
        List<TEntityDto> GetList();

        /// <summary>
        /// 分页显示
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PageResponseModel<TEntityDto> GetPagedList(TPagingDto input);

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        int Insert(TAddUpdateDto input);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        int Update(TAddUpdateDto input);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(Tkey id);

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntityDto GetById(Tkey id);

    }
}
