﻿using System.Linq.Expressions;
using System;
using XfTechOAWeb.Dtos.Notices;
using XfTechOAWeb.Models;
using XfTechOAWeb.Dtos;

namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 消息通知服务接口
    /// </summary>
    public interface INoticeService
    {
        /// <summary>
        /// 添加通知
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        int AddNotice(NoticeAddDto dto);
        /// <summary>
        /// 分页显示
        /// </summary>
        /// <param name="pageDto"></param>
        /// <returns></returns>
        PageResponseModel<NoticeListDto> GetNoticeByPage(NoticePageDto pageDto);
        /// <summary>
        /// 获取id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Notice GetNoticeId(int noticeId);
    }
}
