﻿#region 文件说明
/*
 * 创建人：申永康
 * 创建时间：2022/8/16
 * 
 * *******************************************************************
 * 
 * 功能描述：部门服务
 * 
 * *******************************************************************
 * 修改履历：
 * 申永康 20220816 创建文件
 * 申永康 20220816 添加注释
 * 
 * *******************************************************************
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models;
using XfTechOAWeb.Dtos.Departments;
using XfTechOAWeb.Dtos;


namespace XfTechOAWeb.IService
{
    public interface IDepartmentService
    {
        /// <summary>
        /// 部门添加
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        ResponseModel<AddDepartmentsDto> Add(AddDepartmentsDto department);

        /// <summary>
        /// 部门修改
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        ResponseModel<UptDepartmentsDto> Update(UptDepartmentsDto department);

        /// <summary>
        /// 部门树形显示
        /// </summary>
        /// <returns></returns>
        List<Department> GetDepartTree();

        /// <summary>
        /// 部门分页显示
        /// </summary>
        /// <param name="departmentPageDto"></param>
        /// <returns></returns>
        PageResponseModel<DepartmentDto> DShow(DepartmentPageDto departmentPageDto);

        /// <summary>
        /// 显示
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Department GIt(int id);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Del(string id);

        /// <summary>
        /// 停用
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Upt(int id);

        /// <summary>
        /// 部门添加角色
        /// </summary>
        /// <param name="depId"></param>
        /// <param name="uId"></param>
        /// <returns></returns>
        int AddDepartmentsUser(AddDepartmentsUsersDto input);

        /// <summary>
        /// 用户返填
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        PageResponseModel<BackDepartmentDto> GetDepartment(int id);
    }
}
