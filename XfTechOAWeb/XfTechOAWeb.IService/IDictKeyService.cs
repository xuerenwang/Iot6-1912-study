﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Dicts;
using XfTechOAWeb.IService.Base;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 字典键 应用服务接口
    /// </summary>
    public interface IDictKeyService : IBaseService<int,                //主键类型
                                                    DictKeyDto,         //用于显示的Dto
                                                    DictKeyAddUpdateDto,//用与修改和新增的Dto 
                                                    PageRequestModel>   //用于分页的Dto
    {
        //可以扩展自己的接口方法
    }
}
