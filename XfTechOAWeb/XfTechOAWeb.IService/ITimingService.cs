﻿/*
 * 创建人：金鑫
 * 创建时间：2022/8/26
 * 
 * *******************************************************************
 * 
 * 功能描述：定时任务服务接口
 * 
 * *******************************************************************
 * 修改履历：
 * 
 * *******************************************************************
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models;
using XfTechOAWeb.Dtos.Timing;

namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 定时任务服务接口
    /// </summary>
    public interface ITimingService
    {
        /// <summary>
        /// 查询全部Job任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        PageDto GetTimingList(TimingPage data);

        /// <summary>
        /// 删除数据库Job任务
        /// </summary>
        /// <returns></returns>
        BaseResult DeleteTiming(int id);

        /// <summary>
        /// 添加数据库Job任务
        /// </summary>
        /// <returns></returns>
        BaseResult AddTiming(Timing timing);

        /// <summary>
        /// 将全部Job状态改为0
        /// </summary>
        /// <returns></returns>
        BaseResult CorrectJob();

        /// <summary>
        /// 启动Job任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<BaseResult> StartScheduleJob(TimingDto data);

        /// <summary>
        /// 暂停任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<BaseResult> PauseScheduleJob(DeleteJobDto data);

        /// <summary>
        /// 恢复被暂停的任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<BaseResult> RecoverScheduleJob(StartJobDto data);

        /// <summary>
        /// 反射
        /// </summary>
        /// <returns></returns>
        List<dynamic> After();
    }
}
