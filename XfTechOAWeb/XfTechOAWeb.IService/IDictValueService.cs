﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Dicts;
using XfTechOAWeb.IService.Base;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 字典值 应用服务接口
    /// </summary>
    public interface IDictValueService : IBaseService<int,                   //主键类型
                                                       DictValueDto,         //用于显示的Dto
                                                       DictValueAddUpdateDto,//用与修改和新增的Dto 
                                                       PageRequestModel>      //用于分页的Dto
    {
        //可以扩展自己的接口方法

        /// <summary>
        /// 条件分页查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PageResponseModel<DictValueDto> Query(DictValuePageRequestDto input);

        /// <summary>
        /// 根据字典键查询 字典项列表
        /// </summary>
        /// <param name="dictKeyId"></param>
        /// <returns></returns>
        List<DictValueDto> Query(int dictKeyId);
    }
}
