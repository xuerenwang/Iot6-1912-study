﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Syslog;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.IService
{
    //系统日志服务接口
   public interface IOperationLogService
    {
        //系统日志的显示(查找)操作
        PageResponseModel<OperationLog>  GetPageList(LogsPageRequestDto input);

        //获取日志，不分页
        List<OperationLog> GetList(LogsPageRequestDto input);


        int Add(OperationLog operation);
    }
}
