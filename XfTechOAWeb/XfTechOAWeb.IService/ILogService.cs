﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.IService
{
    public interface ILogService
    {
        int AddLog(LogMessage log);
    }
}
