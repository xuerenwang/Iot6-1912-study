﻿using XfTechOAWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos.TodoItems;

namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 待办事项管理功能模块
    /// </summary>
    public interface ITodoItemService
    {
        Task<List<TodoItem>> GetListAsync(int userId);

        Task<int> InsertAsync(CreateUpdateTodoItemDto todoItem);

    }
}
