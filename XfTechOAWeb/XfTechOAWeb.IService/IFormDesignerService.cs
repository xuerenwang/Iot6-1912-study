﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.Dtos.Forms;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.IService
{
    /// <summary>
    /// 表单设计接口服务
    /// </summary>
    public interface IFormDesignerService
    {
        /// <summary>
        /// 显示所有
        /// </summary>
        /// <returns></returns>
        ResponseModel<PageResponseModel<FormShowDto>> GetAllFormDesiSgner(string tableName,int pageIndex,int pageSize);
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="formDesigner"></param>
        /// <returns></returns>
        int AddFormDesigner(FormDto input);

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ResponseModel<int> UpdateFormDesigner(FormDto input);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResponseModel<int> DeleteFormDesigner(string ids);
        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        Form GetFormDesigner(int id);
    }
}
