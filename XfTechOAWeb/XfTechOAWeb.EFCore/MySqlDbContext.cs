﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore;
using XfTechOAWeb.Models;
using Microsoft.Extensions.Configuration;
using XfTechOAWeb.Infrastructure.Utilities;
using System.Runtime;

namespace XfTechOAWeb.EFCore
{
    /// <summary>
    /// MySql数据库上下文
    /// </summary>
    public class MySqlDbContext : DbContext
    {
        /// <summary>
        /// 数据库上下文
        /// </summary>
        /// <param name="options"></param>
        public MySqlDbContext(DbContextOptions<MySqlDbContext> options) : base(options)
        {
        }

        /// <summary>
        /// 配置连接字符串
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#if DEBUG
            //执行EFCore时会在控制台打印对应的SQL语句
            optionsBuilder.LogTo(Console.WriteLine);
#endif
            if(!optionsBuilder.IsConfigured)
            {
                //读取配置文件中的连接字符串
                string connStr = AppConfigurtaionServices.Configuration["ConnectionStrings:mysql"];

                string connStr2 = AppConfigurtaionServices.Configuration.GetConnectionString("mysql");

                optionsBuilder.UseMySql(connStr, ServerVersion.AutoDetect(connStr));
            }

            base.OnConfiguring(optionsBuilder);
        }

        //DbSet

        /// <summary>
        /// 数据字典主表
        /// </summary>
        public DbSet<DictKey> Dicts { get; set; }
        /// <summary>
        /// 数据字典从表
        /// </summary>
        public DbSet<DictValue> DictDetails { get; set; }
        public DbSet<LogMessage> LogMessages { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<MyMessage> MyMessage { get; set; }
        public DbSet<DepartmentsUser> DepartmentsUsers { get; set; }
        public DbSet<Timing> Timings { get; set; }
        public DbSet<TimingLog> TimingLogs { get; set; }
        public DbSet<OperationLog> operationLog { get; set; }

        public DbSet<Form> FormDesigners { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<Notice>  Notices { get; set; }

    }
}
