﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XfTechOAWeb.EFCore.Migrations
{
    public partial class ModifyDictTableName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_dicts_dicts_DictId1",
                table: "dicts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dicts",
                table: "dicts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dictDetails",
                table: "dictDetails");

            migrationBuilder.RenameTable(
                name: "dicts",
                newName: "Dicts");

            migrationBuilder.RenameTable(
                name: "dictDetails",
                newName: "DictDetails");

            migrationBuilder.RenameIndex(
                name: "IX_dicts_DictId1",
                table: "Dicts",
                newName: "IX_Dicts_DictId1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Dicts",
                table: "Dicts",
                column: "DictId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DictDetails",
                table: "DictDetails",
                column: "DetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dicts_Dicts_DictId1",
                table: "Dicts",
                column: "DictId1",
                principalTable: "Dicts",
                principalColumn: "DictId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dicts_Dicts_DictId1",
                table: "Dicts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Dicts",
                table: "Dicts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DictDetails",
                table: "DictDetails");

            migrationBuilder.RenameTable(
                name: "Dicts",
                newName: "dicts");

            migrationBuilder.RenameTable(
                name: "DictDetails",
                newName: "dictDetails");

            migrationBuilder.RenameIndex(
                name: "IX_Dicts_DictId1",
                table: "dicts",
                newName: "IX_dicts_DictId1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dicts",
                table: "dicts",
                column: "DictId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dictDetails",
                table: "dictDetails",
                column: "DetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_dicts_dicts_DictId1",
                table: "dicts",
                column: "DictId1",
                principalTable: "dicts",
                principalColumn: "DictId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
