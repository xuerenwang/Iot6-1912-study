﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XfTechOAWeb.EFCore.Migrations.MySqlDb
{
    public partial class ModifiedMyMessageModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "MessageReadDate",
                table: "MyMessage",
                type: "datetime(6)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MessageReadDate",
                table: "MyMessage");
        }
    }
}
