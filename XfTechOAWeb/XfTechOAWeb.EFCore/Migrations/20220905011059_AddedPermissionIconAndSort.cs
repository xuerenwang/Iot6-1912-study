﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace XfTechOAWeb.EFCore.Migrations
{
    public partial class AddedPermissionIconAndSort : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsMenu",
                table: "Permissions",
                type: "bit",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "IconName",
                table: "Permissions",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SortNo",
                table: "Permissions",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IconName",
                table: "Permissions");

            migrationBuilder.DropColumn(
                name: "SortNo",
                table: "Permissions");

            migrationBuilder.AlterColumn<int>(
                name: "IsMenu",
                table: "Permissions",
                type: "int",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit");
        }
    }
}
