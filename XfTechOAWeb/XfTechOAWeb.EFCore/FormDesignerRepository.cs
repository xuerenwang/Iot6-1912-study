﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XfTechOAWeb.Data;
using XfTechOAWeb.Dtos;
using XfTechOAWeb.EFCore.Base;
using XfTechOAWeb.Models;

namespace XfTechOAWeb.EFCore
{
    /// <summary>
    /// 表单设计实现仓储
    /// </summary>
    public class FormDesignerRepository : EfBaseRepository<Form>, IFormDesignerRepository
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="db">上下文注入</param>
        public FormDesignerRepository(DbContext db) : base(db)
        {
            _db = db;   
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="formDesigner"></param>
        /// <returns></returns>
        public int AddFormDesigner(Form formDesigner)
        {
            
            _db.Set<Form>().Add(formDesigner);
            return _db.SaveChanges();
        }
       /// <summary>
       /// 删除
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        public int DeleteFormDesigner(string ids)
        {
            string[] id = ids.Split(',');
            foreach (var item in id)
            {
                _db.Set<Form>().Remove(_db.Set<Form>().FirstOrDefault(x => x.Id == Convert.ToInt32(item)));
            }
            
            return _db.SaveChanges();   
        }
        /// <summary>
        /// 显示
        /// </summary>
        /// <returns></returns>
        public List<Form> GetAllFormDesiSgner()
        {
           return  _db.Set<Form>().ToList();   
        }
        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Form GetFormDesigner(int id)
        {
            return _db.Set<Form>().FirstOrDefault(x=>x.Id==id);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="formDesigner"></param>
        /// <returns></returns>
        public int UpdateFormDesigner(Form formDesigner)
        {
            _db.Set<Form>().Update(formDesigner);
            return _db.SaveChanges();
        }
    }
}
