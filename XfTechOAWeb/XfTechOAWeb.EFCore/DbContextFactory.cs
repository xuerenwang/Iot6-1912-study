﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XfTechOAWeb.Infrastructure.Utilities;

namespace XfTechOAWeb.EFCore
{
    /// <summary>
    /// 上下文工厂
    /// </summary>
    public class DbContextFactory
    {
        static DbContext _dbcontext;

        //读取配置文件
        public static DbContext CreateDbContext()
        {
            //读取配置文件来获得 要连接的数据库类型
            string sqlType = AppConfigurtaionServices.Configuration["SqlType"];

            //单例模式
            if (_dbcontext != null)
            {
                return _dbcontext;
            }

            if (sqlType.Equals("MySql"))
            {
                DbContextOptions<MySqlDbContext> options = new DbContextOptions<MySqlDbContext>();
                _dbcontext = new MySqlDbContext(options);
            }
            else if (sqlType.Equals("SqlServer"))
            {
                DbContextOptions<SqlServerDbContext> options = new DbContextOptions<SqlServerDbContext>();
                _dbcontext = new SqlServerDbContext(options);
            }

            return _dbcontext;
        }

    }
}
