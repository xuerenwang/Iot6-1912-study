﻿using Microsoft.EntityFrameworkCore;
using XfTechOAWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using XfTechOAWeb.Infrastructure.Utilities;

namespace XfTechOAWeb.EFCore
{
    /// <summary>
    /// SqlServer数据库上下文
    /// </summary>
    public class SqlServerDbContext : DbContext
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="options"></param>
        public SqlServerDbContext(DbContextOptions<SqlServerDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// 数据字典主表
        /// </summary>
        public DbSet<DictKey> Dicts { get; set; }
        /// <summary>
        /// 数据字典从表
        /// </summary>
        public DbSet<DictValue> DictDetails { get; set; }
        public DbSet<LogMessage> LogMessages { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<MyMessage> MyMessage { get; set; }
        public DbSet<DepartmentsUser> DepartmentsUsers { get; set; }
        public DbSet<Timing> Timings { get; set; }
        public DbSet<TimingLog> TimingLogs { get; set; }
        public DbSet<OperationLog> operationLog { get; set; }

        public DbSet<Form> FormDesigners { get; set; }
        public DbSet<TodoItem>  TodoItems { get; set; }
        public DbSet<Notice> Notices { get; set; }


        /// <summary>
        /// 上下文配置
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!optionsBuilder.IsConfigured)
            {
                //读取配置文件中的连接字符串
                string connStr = AppConfigurtaionServices.Configuration.GetConnectionString("sqlserver");

                optionsBuilder.UseSqlServer(connStr);
            }

            //方法一
            // 配置标准日志，执行EFCore时会在控制台打印对应的SQL语句
            //ILoggerFactory myLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
            //optionsBuilder.UseLoggerFactory(myLoggerFactory);


            //方法二，执行EFCore时会在控制台打印对应的SQL语句
            //#if Debug的作用叫做条件编译，如果是DEBUG开发环境，则起作用，否则就等于没有这些代码
#if DEBUG
            optionsBuilder.LogTo(Console.WriteLine);
#endif

            base.OnConfiguring(optionsBuilder);
        }

        /// <summary>
        /// 重写父类的OnModelCreating虚方法
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //在重写的方法里面 去配置实体类型到 数据库表之间的映射规则

            //用于配置映射规则的一些列的方法，叫做FluentApi
            //modelBuilder.Entity<User>().ToTable("Users");
            //modelBuilder.Entity<User>().Property(x => x.Account).HasMaxLength(60).IsRequired();
            //modelBuilder.Entity<User>().Property(x => x.Password).HasMaxLength(60).IsRequired();
            //modelBuilder.Entity<User>().Property(x => x.UName).HasMaxLength(50).IsRequired();

            base.OnModelCreating(modelBuilder);
        }

    }
}
