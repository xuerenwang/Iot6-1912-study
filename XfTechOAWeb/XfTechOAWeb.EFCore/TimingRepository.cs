﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore; //应用ef core的命名空间
using System.Data;
using XfTechOAWeb.Data;
using XfTechOAWeb.Models;
using XfTechOAWeb.Dtos.Timing;
using XfTechOAWeb.EFCore.Base;

namespace XfTechOAWeb.EFCore
{
    public  class TimingRepository :EfBaseRepository<Timing>, ITimingRepository
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="db">上下文注入</param>
        public TimingRepository(DbContext db) : base(db)
        {
            _db = db;
        }

        public bool UptZt(int id, int status)
        {
            Timing data = _db.Set<Timing>().Where(x => x.Id.Equals(id)).FirstOrDefault();
            if (data == null)
            {
                return false;
            }
            data.Zt = status; 
            var dd = _db.SaveChanges() > 0;
            return dd;
        }

        bool ITimingRepository.Finish(string GroupName)
        {
            string[] data = GroupName.Split('.');
         Timing TimingData =  _db.Set<Timing>().Where(x => x.JobGroup.Equals(data[0]) && x.JobName.Equals(data[1])).FirstOrDefault();
            TimingData.Zt = 0;
            return _db.SaveChanges() > 0;
        }

        bool ITimingRepository.Insert(TimingLog Logs)
        {
                _db.Set<TimingLog>().Add(Logs);
            return  _db.SaveChanges() > 0;
        }

        bool ITimingRepository.Remove(int id)
        {
            _db.Set<Timing>().Remove(_db.Set<Timing>().Where(x=>x.Id.Equals(id)).FirstOrDefault());
          return  _db.SaveChanges()>0;
           
        }
       
    }
}
