import Vue from 'vue';
import App from './App.vue';
import router from './router';   //导入src/router/index.js的Router路由对象
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
import { messages } from './components/common/i18n';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
// import './assets/css/theme-green/index.css'; // 浅绿色主题
import './assets/css/icon.css';
import './components/common/directives';
import 'babel-polyfill';

import store from "./store";


///////////////////////
import axios from "axios";
axios.defaults.baseURL= process.env.NODE_ENV === 'development'? "http://localhost:5000/api" :'http://120.25.237.149:8083/api/'; //基地址
Vue.prototype.$axios=axios;

import VForm from 'vform-builds' //引入VForm库 
import 'vform-builds/dist/VFormDesigner.css' //引入VForm样式 
Vue.use(VForm) //全局注册VForm(同时注册了v-form-designer和v-form-render组件)
/////////////////////

Vue.config.productionTip = false;
Vue.use(VueI18n);
Vue.use(ElementUI, {
    size: 'small'
});
const i18n = new VueI18n({
    locale: 'zh',
    messages
});

//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    //设置网站的标题
    document.title = `${to.meta.title} | 智慧低代码开发平台`;

    //读取登录功能所保存的用户session信息
    const role = sessionStorage.getItem('loginUserInfo'); 
    //console.log(role)
    if (!role && to.path !== '/login') {
        next('/login');     //跳转到/login
    } else if (to.meta.permission) { //判断是否设置了需要权限才能访问
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        role === 'admin' ? next() : next('/403');

        //判断 要访问的页面 是否被包含在 张三这个人能访问的页面权限之中
        //todo:待实现

    } else {
        // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
        if (navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor') {
            Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
                confirmButtonText: '确定'
            });
        } else {
            next();
        }
    }
});

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app');
