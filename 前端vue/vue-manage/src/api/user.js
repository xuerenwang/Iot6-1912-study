/*
*用户模块  文件src/api/user.js
 */
import request from '@/utils/request';  //引用工具模块 request.js

//获取用户列表
export const getUsers = query => {
    return request({
        url: '/api/User',
        method: 'get',
        params: query
    });
};

//根据主键获取用户信息
export const findUser = query => {
    return request({
        url: '/api/User/id',
        method: 'get',
        params: query
    });
};

//获取用户信息
export const UserFind = query => {
    return request({
        url: '/api/User/userid',
        method: 'get',
        params: query
    });
};

//添加用户信息
export const addUser = query => {
    return request({
        url: '/api/User',
        method: 'post',
        data: query
    });
};

//批量删除用户信息
export const deleteUser = query => {
    return request({
        url: '/api/User',
        method: 'delete',
        params: query
    });
};

//修改用户信息
export const updateUser = data => {
    return request({
        url: '/api/User',
        method: 'put',
        data: data
    });
};

//添加用户角色关系
export const addUserRole = data => {
    return request({
        url: '/api/User/urole',
        method: 'post',
        data: data
    });
};

//修改密码
export const PasswordUpt = data => {
    return request({
        url: '/api/User/password',
        method: 'put',
        data: data
    });
};