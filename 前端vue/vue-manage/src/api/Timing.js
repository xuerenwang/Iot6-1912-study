import request from '@/utils/request';


///获取数据库Job信息
export const getTiming = query => {
    return request({
        url: '/api/Timing/Timing',
        method: 'get',
        params: query
    });
};
///删除数据库单个Job信息
export const deleteTiming = (query) => {
    return request({
        url: '/api/Timing/Timing',
        method: 'delete',
        params:query 
    });
};
///对数据库添加Job信息
export const addTiming = data => {
    return request({
        url: '/api/AddTiming',
        method: 'post',
        data: data
    });
};

///启动Job任务
export const StartJob = data => {
    return request({
        url: '/api/Timing/ScheduleJob',
        method: 'post',
        data: data
    });
};
//暂停Job任务
export const SuspendJob = data => {
    return request({
        url: '/api/Timing/ScheduleJob',
        method: 'delete',
        data: data
    });
};
//恢复Job任务
export const RecoverJob= data => {
    return request({
        url: '/api/Timing/ScheduleJob',
        method: 'put',
        data: data
    });
};
//反射拿取命名路由
export const AfterRouting = () => {
    return request({
        url: '/api/Timing/AfterRouting',
        method: 'get',
    });
};
//矫正Job信息
export const CorrectJob = () => {
    return request({
        url: '/api/Timing/CorrectJob',
        method: 'get',
    });
};


