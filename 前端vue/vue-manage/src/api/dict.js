/**
 * 字典管理
 */
import request from '../utils/request';

//显示字典分类值
export const DictDetails = query => {
    return request({
        url: 'api/DictValue/getList',
        method: 'get',
        params: query
    });
};
//添加字典值
export const AddDictValue = form => {
    return request({
        url: 'api/DictValue/',
        method: 'post',
        data: form
    });
};
//修改字典值
export const EditDictValue = form => {
    return request({
        url: 'api/DictValue/',
        method: 'put',
        data: form
    });
};

//删除字典值
export const DeleteDictValue = id => {
    return request({
        url: `api/DictValue/${id}`,
        method: 'delete',
    });
};

//删除字典分类
export const DictDelete = id => {
    return request({
        url: `api/DictKey/${id}`,
        method: 'delete',

    });
};
//获取字典键
export const GetDict = id => {
    return request({
        url: `api/DictKey/getById/${id}`,
        method: 'get',
    });
};
//显示字典分类
export const DictShow = () => {
    return request({
        url: 'api/DictKey/getList',
        method: 'get',

    });
};
//添加字典
export const DictAdd = form => {
    return request({
        url: 'api/DictKey',
        method: 'post',
        data: form
    });
};
//更新
export const DictUpdate = form => {
    return request({
        url: 'api/DictKey',
        method: 'put',
        data: form
    });
};