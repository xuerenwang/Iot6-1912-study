import request from '../utils/request';

export const LogShow = query => {
    return request({
        url: '/api/SysLogs',
        method: 'get',
        params: query
    });
};


// //导出日志
// export const exportLogs = query => {
//     return request({
//         url: 'api/SysLogs/Export',
//         method: 'get',
//         params: query,
//         responseType: 'blob'  //必须指定blob方式，用于下载文件流
//     });
// };
