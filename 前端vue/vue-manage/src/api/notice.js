/*
*消息通知模块
 */
import request from '@/utils/request';  //引用工具模块 request.js

//获取我的消息列表
export const getNoticeList = query => {
    return request({
        url: `/api/Notice`,
        method: 'get',
        params: query
    });
};
// 发送通知公告
export const sendNotice = query => {
    return request({
        url: `/api/Notice/SendNotice`,
        method: 'get',
        params: query
    });
};
//添加信息
export const addNotice = data => {
    return request({
        url: '/api/Notice',
        method: 'post',
        data: data
    });
};
