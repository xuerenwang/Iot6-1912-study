import request from '@/utils/request';  //导入帮助类里面的request对象，帮助我们发送请求给后端api接口

//用户登录 （定义了一个loginUser方法，给后端api发送请求）
export const loginUser = data => {
    return request({
        url: '/api/Account/Login',
        method: 'post',  //post请求用data
        data: data
    });
};


//刷新Token
export const getNewToken = query => {
    return request({
        url: '/api/Account/RefreshAccessToken',
        method: 'get', //get请求用params
        params: query

    });
};

//菜单列表
export const getTreeMenus = () => {
    return request({
        url: '/api/Account/GetMenus',
        method: 'get',
    });
};