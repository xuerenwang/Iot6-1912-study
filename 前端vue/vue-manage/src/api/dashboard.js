import request from '../utils/request';

//加载用户的待办事项
export const getTodoList = () => {
    return request({
        url: `api/TodoItem/`,
        method: 'get'
    });
};
//添加部门
export const TodoListAdd = query => {
    return request({
        url: '/api/TodoItem',
        method: 'post',
        data: query
    });
};
