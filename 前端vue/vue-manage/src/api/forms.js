/*
*角色模块
 */

import request from '@/utils/request';

//获取表单列表
export const GetFormDesigners = (query) => {
    return request({
        url: '/api/Form',
        method: 'get',
        params:query,
    });
};

//删除表单列表
export const DelFormDesigners = (query) => {
    return request({
        url: '/api/Form',
        method: 'delete',
        params:query,
    });
};

//反填表单列表
export const RefillFormDesigners = (query) => {
    return request({
        url: '/api/Form/FindSingle',
        method: 'get',
        params:query,
    });
};



//修改表单
export const UpdateFormDesigners =query => {
    return request({
        url: '/api/Form',
        method: 'put',
        data:query
    });
};


//添加表单
export const CreateForm = data => {
    return request({
        url: '/api/Form',
        method: 'post',
        data: data
    });
};
