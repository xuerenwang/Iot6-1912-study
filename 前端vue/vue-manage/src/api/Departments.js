/**
 * 部门管理模块
 */
import request from '../utils/request';

//获取左侧部门树
export const DeparTreeShow = query => {
    return request({
        url: 'api/Department/getTree',
        method: 'get',
        params: query
    });
};

//获取部门列表
export const DeparShows = (pindex, psize, did, names) => {
    return request({
        url: 'api/Department?Pindex=' + pindex + "&Psize=" + psize + "&did=" + did + "&names=" + names,
        method: 'get',
        params: (pindex, psize, did, names)
    });
};

//根据主键获取单个部门信息
export const Deparback = (ids) => {
    return request({
        url: `/api/Department/${ids}`,
        method: 'get',
    });
};

//添加部门
export const DeparAdd = query => {
    return request({
        url: '/api/Department',
        method: 'post',
        data: query
    });
};

//修改部门
export const DeparUpdate = query => {
    return request({
        url: '/api/Department',
        method: 'put',
        data: query
    });
};

//删除部门
export const DeparDel = query => {
    return request({
        url: `/api/Department/${query}`,
        method: 'Delete',
        data: query
    });
};

//更新部门
export const DeparUpt = query => {
    return request({
        url: `/api/Department/disable/${query}`,
        method: 'Put',
        data: query
    });
};

//添加部门用户关系
export const DepartmentsUser = (depId, uIds) => {
    return request({
        url: '/api/Department/addDepartmentUsers',
        method: 'post',
        data: {
            depId, uIds
        }
    });
};

//返回某部门下的员工列表
export const BackDepartment = query => {
    return request({
        url: `/api/Department/${query}/getUsers`,
        method: 'get',
    });
};