/*
*权限模块
 */
import request from '../utils/request';//引用工具模块 request.js

//获取权限列表
export const getPermissionTree = query => {
    return request({
        url: '/api/Permission',
        method: 'get',
        params: query
    });
};

//权限的添加
export const addPermission = query => {
    return request({
        url: '/api/Permission',
        method: 'post',
        data:query
    });
};

//批量删除权限信息
export const deletePermission = query => {
    return request({
        url: `/api/Permission/${query}`,
        method: 'delete'
    });
};

//权限的编辑
export const updatePermission =query => {
    return request({
        url: '/api/Permission',
        method: 'put',
        data:query
    });
};

//根据主键查询权限
export const getPermission = query => {
    return request({
        url: `/api/Permission/${query}`,
        method: 'get'
    });
};

//获取权限树形列表
// export const TreeAuthority = query => {
//     return request({
//         url: '/api/Permission/pid',
//         method: 'get',
//         params:query
//     });
// };