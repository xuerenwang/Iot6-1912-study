/*
*我的消息模块
 */
import request from '@/utils/request';  //引用工具模块 request.js

//获取我的消息列表
export const getMessages = query => {
    return request({
        url: '/api/SysMessage',
        method: 'get',
        params: query
    });
};

// //根据主键获取我的消息
// export const findMessages = query => {
//     return request({
//         url: '/api/SysMessage/GetSysMessageId?id='+query,
//         method: 'get',
//         params: query
//     });
// };

//获取有几条未读消息
export const getUnReadCount = () => {
    return request({
        url: 'api/SysMessage/GetUnReadCount',
        method: 'get'
    });
};
//删除消息
export const MsgDel = data => {
    return request({
        url: `/api/UserMsg`,
        method: 'Delete',
        data: data
    });
};
//标记已读（可用）
export const uptMsgStatus = (id,status) => {
    return request({
        url: `/api/SysMessage/${id}?status=${status}`,
        method: 'put'
    });
};
