/*
*角色模块
 */

import request from '@/utils/request';

//角色添加
export const addRole = query => {
    return request({
        url: '/api/Role',
        method: 'post',
        //headers:{'Authorizaiton:':'Bearer ' + 令牌 },
        data: query
    });
};

//角色列表
export const getRoleList = query => {
    return request({
        url: '/api/Role',
        method: 'get',
        params: query
    });
};

//角色删除
export const deleteRole = query => {
    return request({
        url: '/api/Role',
        method: 'delete',
        data: query
    });
};

//角色修改
export const updateRole = query => {
    return request({
        url: '/api/Role',
        method: 'put',
        data: query
    });
};

//角色添加权限
export const addTreeAuthority=query=>{
    return request({
        url: '/api/Role/addPermission',
        method: 'post',
        data: query
    });
}

//权限
export const getTreeAuthority=query=>{
    return request({
        url: 'api/Permission/',
        method: 'get',
        params: query
    });
}

//根据角色查权限
export const getPer=query=>{
    return request({
        url: '/api/Role/'+query,
        method: 'get',
    });
}

//角色名唯一验证
export const uniqueRName=query=>{
    return request({
        url: '/api/Role/UniqueRName/'+query.toString(),
        method: 'get',
    });
}