
const getters = {
    token: state => state.token,
    name: state => state.name,
    loginUser: state => state.userLoginInfo,
    refreshToken: state => state.refreshToken,
  }
  export default getters