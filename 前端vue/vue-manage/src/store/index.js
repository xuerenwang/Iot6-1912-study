import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'

Vue.use(Vuex)
//这里定义的数据是全局变量
const store = new Vuex.Store({
  state: {      //state相当于vue页面中定义的data
    userLoginInfo: window.sessionStorage.getItem("loginUserInfo"), //获取用户登录信息
    token: window.sessionStorage.getItem("token"),   //获取令牌
    name: window.sessionStorage.getItem("userName"),
    refreshToken:window.sessionStorage.getItem("refreshToken"),
  },
  mutations: {  //可变值    在vue组件中用 this.$store.commit('setLoginInfo',user)
    setLoginInfo (state, userLoginInfo) {
      state.userLoginInfo = userLoginInfo;
      state.name = userLoginInfo.uName;

      //同时保存到会话缓存中
      window.sessionStorage.setItem("loginUserInfo", JSON.stringify(userLoginInfo)); 
      window.sessionStorage.setItem("userName", userLoginInfo.uName);  
    },
    setToken (state, token) {
      state.token= token;
      window.sessionStorage.setItem("token",token);    //同时保存到会话缓存中
    },//刷新凭证
    setRefreshToken (state,key) {
      state.refreshToken= key;
      window.sessionStorage.setItem("refreshToken",key);    //同时保存到会话缓存中
    }
  },
  getters
})

export default store