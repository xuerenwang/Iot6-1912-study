/*
*本模块的作用是对axios的封装
 */
import axios from 'axios';
import store from '@/store'

import router from '../router/index.js'
import {getNewToken} from '../api/login.js'    //账户登录模块

//创建一个axios的实例（参考axios的中文官网）
const service = axios.create({
  // process.env.NODE_ENV === 'development' 来判断是否开发环境
  // easy-mock服务挂了，暂时不使用了
  baseURL: process.env.NODE_ENV === 'development'?'http://localhost:5000/':'http://120.25.237.149:8083/',//基地址
  timeout: 5000 * 1000,
});

//前端的AOP编程
//请求拦截器  (拦截下来之后加一些统一的处理程序)
service.interceptors.request.use(
  config => {
    //所有请求携带上JWT token
    if (store.getters.token) {
      //console.log(store.getters.token)

      // let each request carry token
      config.headers['Authorization'] = 'Bearer ' + store.getters.token  // Authorization: Bearer xxxxx.xxxx.xxxxx
    }

    return config;
  },
  error => {
    console.log(error);
    return Promise.reject();
  }
);

//响应拦截器
// service.interceptors.response.use(
//     response => {
//         if (response.status === 200) {
//             return response.data; //这里返回的是data
//         } else {
//             Promise.reject();
//         }
//     },
//     error => {
//         console.log(error);
//         return Promise.reject();
//     }
// );

// 响应拦截器
service.interceptors.response.use(
  response => {
    //console.log('响应拦截器', response)
    return response.data
  }, 
  async function (error) {
    // 如果发生了错误，判断是否是401
    
    if (error.response.status === 401) {
      // 开始处理
      console.log('响应拦截器-错误-401')

      //判断401原因（是否有Token）
      const refreshToken = store.getters.refreshToken
      // console.log("获取刷新Token", refreshToken)
      if (refreshToken) {
        // 1. 请求新token
        try {
          const res =  getNewToken({ rtoken: refreshToken })
              .then((res => { 
                if(res.code>0){
                  // 2. 保存到vuex
                  store.commit('setRefreshToken',res.refreshToken)
                  store.commit('setToken', res.tokenInfo.access_token)
                }
              }))

          // 3. 重发请求
          // service是上面创建的axios的实例，它会自动从vuex取出token带上
          return service(error.config)

        } catch (err) {
          // 清除token
          store.commit('setRefreshToken',{},'setToken', {})
          // 去到登录页(如果有token值，就不能到login)
          const backtoUrl = encodeURIComponent(router.currentRoute.fullPath)
          router.push('/login?backto=' + backtoUrl)
          return Promise.reject(err)
        }
      } else {
        // 去到登录页
        // 清除token
        store.commit('setRefreshToken',{},'setToken', {})
        //const backtoUrl = encodeURIComponent(router.currentRoute.fullPath)
        //router.push('/login?backto=' + backtoUrl)
        router.push('/login')
        return Promise.reject(error)
      }
    } else if(error.response.status === 403 ){   //没有权限访问页面,则跳转到403错误页面
      router.push('/403');
    }
    else {
      return Promise.reject(error)
    }
})

export default service;
